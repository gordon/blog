Title: La nécessité des commentaires
Permalink: la-necessite-des-commentaires
Category: Hacktivisme
Date: 2013-05-30
Modified: 2013-05-30
Tags: blog, commentaires, décentralisation, pelican

Je suis tombé sur cet
[article](http://www.jeremyscheff.com/2011/08/jekyll-and-other-static-site-generators-are-currently-harmful-to-the-free-open-source-software-movement/),
expliquant en quoi les générateurs de blogs statiques, comme
[Pelican](http://getpelican.com) (utilisé ici même) étaient un danger pour
le logiciel libre. En gros, c’est centré sur l’absence de commentaires, hormis
en passant par un service tiers intégré en javascript (comme disqus). Je ne suis
absolument pas d’accord avec ses arguments, et, suite à un débat avec l’ami
[Taziden](http://www.libre-parcours.net/), je pense nécessaire d’expliquer mon
point de vue, au sujet des commentaires et le reste.

<!-- cut -->

Pour commencer, et par honnêté, je dois bien dire que le choix de me passer de
commentaires a été en partie imposé par le choix technologique de Pelican. En
partie seulement, parce que c’est moi qui ai fait ce choix, en connaissance de
cause.

### Revenons aux principes du blog

Aujourd’hui, tout internaute lisant des blogs est habitué à pouvoir laisser son
avis sur un billet en bas de celui-ci. Cette habitude provient des moteurs de
blog très utilisés, comme [Wordpress](http://wordpress.org), ayant intégré cette
fonctionnalité. Mais ne cédons pas à la généralisation technologique. Je me
souviens encore de l’époque où « un blog » signifiait un truc sur
« .skyblog.com », la fameuse plateforme infestée de contenus d’une médiocrité
infinie. Aujourd’hui, on pourrait remplacer Skyblog par Wordpress. On a
indéniablement évolué, étant donné que Wordpress est un logiciel libre, et
qu’une importante partie des blogs existants sous cette plateforme sont hébergés
sur des serveurs persos. Mais dans l’idée, on tend quand même vers cette
généralisation : un blog a nécessairement des commentaires, comme un moteur de
recherche, des catégories, mots-clés, etc.

Mais pourquoi serait-ce le cas ? Un blog, ce n’est rien de plus qu’un journal
personnel. Un site web rédactionnel, peu importe que le contenu soit de la
veille technologique, des opinions politiques, ou des créations artistiques. En
fait, c’est un espace d’expression pour l’auteur, qui en fait ce qu’il veut. Les
moyens techniques pour arriver là sont : le protocole HTTP, et le langage HTML.
Le web, en somme, ni plus ni moins. Un simple éditeur de texte, un <span
title="pan">coin</span> sur un serveur web, et on peut écrire une page basique,
et du contenu à l’intérieur. On a un blog, pas moins vrai que le wordpress du
voisin ou celui-ci. Un blog n’a donc pas à avoir de fonctionnalités
particulières.

### La valeur des échanges

Évidemment, je ne cherche pas à nier l’importance des commentaires de blog,
parfois bien plus intéressants que l’article lui-même, souvent enrichissants,
pour l’auteur et les lecteurs, qui assistent alors à la continuité d’un éventuel
débat lancé par le billet, ou des corrections, ajouts, etc…

Mais il s’agit de quelque chose qu’on ne remet pas en question. Les commentaires
vont sous l’article, et (Wordpress y a beaucoup contribué) ils sont structurés
de façon conventionnelle : un fil de messages chronologiquement ordonnés, avec
parfois des avatars récupérés depuis un service tiers fermé (dont le traitement
des données personnelles est
[douteux](http://techthinker.com/gravatar-privacy-concern/)), parfois avec la
possibilité de répondre à un commentaire particulier… Cette dernière
fonctionnalité est intéressante, parce qu’elle permet à la discussion d’évoluer.
D’un échange entre l’auteur et ses lecteurs, on peut obtenir un débat autonome,
mais restant posté sur le blog.

L’avis de Taziden là-dessus est qu’il est indispensable pour un blog de proposer
une fonctionnalité de commentaires. Pour moi, c’est plus une habitude tenace
qu’un besoin. Pourquoi ne remettrait-on pas plutôt en question cette
fonctionnalité, pour la faire évoluer ? Pourquoi, au lieu de commentaires, ne
pas développer un système d’annotations collaboratives, fonctionnant telles des
calques sur le billet, pour enrichir l’article directement, comme on le peut le
faire avec [Etherpad](https://pad.naohack.org) ? Il s’agit d’une simple idée
d’évolution, et je regrette que si peu de projets aient le courage de remettre à
plat les usages et habitudes pour proposer quelque chose de novateur (en cela,
le projet [Discourse](http://discourse.org) m’intéresse énormément), plutôt que
de se sentir obligés d’intégrer les fonctionnalités « habituelles ».

Malgré cela, un blog reste un espace personnel. C’est l’espace de l’auteur.
Celui-ci ne souhaite pas nécessairement que ses articles soient commentés. Il
arrive d’ailleurs parfois d’écrire pour soi-même, et pas pour être lu. Les
commentaires sous un billet sont quelque chose que l’auteur peut proposer. C’est
un service supplémentaire. Souvent très enrichissant, mais toujours optionnel.
Il n’y a pas, je pense, lieu d’exiger de quelqu’un qu’il mette à disposition un
tel service, sous prétexte qu’il blogue. Et c’est tout à fait normal, un
blog n’est pas un service public ou quelque chose de démocratique.

### INTERNEEEEET

J’ai une confidence à vous faire : mon blog permet les commentaires. D’ailleurs,
il en a régulièrement. Le fait qu’ils ne soient pas nécessairement écrits sous
chaque billet ne les rend pas inexistants ou invisibles.

Après la publication d’une grande partie de mes billets, il m’arrive de recevoir
un mail (chiffré ♥) contenant des remarques sur celui-ci, et avec un patch
attaché, contenant des corrections de forme. Je trouve ça génial, et ce n’est
pas le genre de chose qui aurait été possible avec un Wordpress. Mais encore,
lorsque je publie, je diffuse l’article sur plusieurs canaux, comme IRC ou
Statusnet (qui est ensuite renvoyé sur Twitter, qui est fermé), ou encore des
mailing-lists. Là, les retours se font, les discussions, débats ou trolls, selon
le sujet, se lancent. Et je n’ai aucun contrôle dessus. C’est quelque chose de
fondamental. Sur mon blog, je peux valider, dévalider, supprimer, bannir
quelqu’un, parce que j’en ai le contrôle (après tout, c’est *mon* espace). Mais
une des grandes forces d’Internet, c’est que la communication est possible,
quel qu’en soit le moyen. Je suis fortement attaché à la liberté d’expression,
vous le savez. Et je considère simplement qu’on est plus libre de discuter par
le moyen ou protocole qu’on veut, plutôt que de façon centralisée sur un site.

#### Disqus ? CÉMAL !

À ce titre, la solution proposée, notamment par Pelican, pour permettre les
commentaires, est généralement le service Disqus. Il s’agit d’une application
chargée en javascript qui va permettre de commenter un article via un service
externe. Ce service est totalement centralisé, fermé, et sous son propre
contrôle. J’ignore si un blogueur a la possibilité de supprimer un commentaire
posté sur disqus, mais je sais que la plateforme elle-même a ce droit. Et je ne
sais pas qui est derrière. Qu’est-ce qui me garantit que Disqus ne considèrera
pas unilatéralement que mon commentaire est un spam, ou contrevient à son
éthique ?

L’auteur de l’article évoqué plus haut appelle au développement d’un équivalent
libre à Disqus. À cela, je réponds « NOPE NOPE NOPE NOPE ». Un service tiers
centralisé, qu’il soit libre ou non, c’est mal. Ne serait-ce que parce que, peu
importe sa licence, je ne connais pas les personnes qui s’en occupent. Je n’ai
aucune garantie sur le service : est-ce qu’il risque de tomber en panne et ainsi
rendre inopérables les commentaires sur un grand nombre de blogs ? Est-ce que
les données sont sécurisées ? Est-ce que le service ne risque pas de fermer du
jour au lendemain ?

Ceci dit, je suis mauvaise langue. Rien n’empêche la création d’un service de
commentaires décentralisé, que l’on pourrait installer sur son propre serveur
pour servir ses propres commentaires. Ce serait une solution acceptable. Mais
l’inclusion dans le blog se ferait en javascript, ce qui n’est pas très
accessible. Je préfère me passer de javascript autant que possible, donc je
n’utiliserais vraisemblablement pas une telle solution.

Au lieu de cela, je laisse les retours se faire naturellement. On est sur
Internet, bon sang, les moyens de communication ne manquent pas. En réagissant
par le biais de protocoles plus adaptés pour ça, on ne dépend plus du blog, de
son auteur, d’un service tiers, et de tous les risques de censure en découlant.

Un autre avantage indirect est la possibilité de développer son avis par un
autre billet de blog qui se présentera en réponse du premier. Cela favorise la
discussion, qui est alors d’autant plus profonde qu’elle ne pourrait l’être en
140 caractères. Mon [dernier
billet](http://gordon.re/feminisme/a-ceux-qui-ralent.html) est dans ce cas : je
voulais répondre en commentaire à Numendil, mais il les avait désactivés, alors
j’ai pris le temps de faire un billet complet.

### On n’est pas dans une salle de conférence

On m’a rétorqué qu’il était important que les commentaires soient proposés par
le blog lui-même, en faisant le parallèle avec une conférence, où le public est
invités à la fin à poser des questions. Cela n’a, pour moi, pas plus de sens que
la tristement célèbre comparaison entre Internet et une autoroute. Les
contraintes physiques n’ont juste rien à voir. Une conférence se déroule dans un
lieu clos et défini, à un moment défini, et les personnes, pour se faire
entendre, parlent l’une après l’autre, et s’adressent au conférencier. Un blog
n’a aucune de ces contraintes. Par ailleurs, à certaines conférences auxquelles
j’ai pu participer (comme [Passage En
Seine](http://passageenseine.org)), les retours se faisaient aussi sur IRC, donc
leur décentralisation est déjà possibe IRL, donc je ne vois pas le problème de
laisser les retours se faire ailleurs que sous l’article.

### Bad-buzz friendly

Il n’y a pas que de gentils barbus *(visuel non contractuel)* qui s’expriment
sur les Internets. Il y a aussi des politiques vaguement pourris, des boîtes qui
cherchent à nous plumer sans trop que ça se voie, bref des gens qui ont un
intérêt économique (entre autres) à ce qu’on ne leur crache pas trop bruyamment
dessus. Et donc qui n’hésiteront pas à modérer sévèrement les moyens de
communications qu’ils daignent mettre en place, pour éviter tout débordement ou
diffusion d’une mauvaise image. Dans ce cas, l’utilisation de canaux tiers est
parfaitement indispensable, parce que certaines choses méritent d’être sues. Si
on se limite aux moyens autorisés, ce n’est plus du *minitel 2.0* qu’on utilise,
mais de la *télé 2.0*, où les diffuseurs sélectionnent ce qui a le droit d’être
dit. C’est un modèle du passé, et il faut absolument éviter de restreindre la
communication sur Internet à ces espaces de discussion qu’on veut bien nous
laisser.

Donc non, un blog statique ne nuit pas au logiciel libre. À l’inverse, il
encourage à l’utilisation épanouie d’Internet, c’est à dire à la
décentralisation des échanges, à l’utilisation de protocoles prévus pour ça. 
