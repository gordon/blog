Title: [Traduction] Telecomix
Permalink: traduction-telecomix
Date: 2012-05-10
Modified: 2012-05-10
Tags: méduses, okhin, poulpes, telecomix, traduction
Category: Hacktivisme

*Comme pour les précédents billets, il s’agit d’une traduction d’[Okhin](https://about.okhin.fr/posts/Telecomix/).*

<!-- cut -->

### Précompilation

Ça fait presque deux ans que je traîne avec la géniale équipe composée de gens et méduses qu’est [Telecomix](https://telecomix.org/), et je pense que c’est la première fois que j’écris à ce sujet. J’en ai beaucoup discuté récemment, principalement parce que pas mal de médias veulent discuter avec nous, mais aussi parce que j’ai entendu parler d’au moins deux autres projets à long terme au sujet des Hacktivistes.

De plus, nous avons eu une intéressante discussion avec l’équipe « de base », à propos des tenants et aboutissants du cluster, accompagnée d’un nombre grandissant de questions venant des gens.

Ce qui donne ce billet. Bien sûr, étant donné que Telecomix est la somme des gens à l’intérieur, ce n’est pas la vision d’un seul esprit, mais plutôt une partie de cette hydre gélatineuse.

### Suivre le lapin blanc

On me pose souvent une question : comment est-ce que je me suis retrouvé dans Telecomix ? Je réponds généralement que c’est simplement arrivé. Je ne cherchais pas à entrer dans un tel groupe de personnes. Je ne pense pas que quiconque ayant un esprit sain serait volontaire pour rejoindre un groupe qui boufferait ses jours et ses nuits, attirerait l’attention sur lui dans des situations non souhaitées (et je ne parle pas des médias), augmenterait les attentes que les gens ont de lui, et le forcerait à faire des choix difficiles (aller se coucher ou empêcher des gens de se faire tuer).

Si vous le présentez comme ça, personne ne l’acceptera. À part quelques prétendus héros, peut-être (mais les héros sont des sociopathes de toutes façons).

Alors, j’ai fini avec Telecomix au même moment où j’ai décidé de rejoindre un hackerspace. je suis entré là-dedans en rencontrant un tas de personnes. Le nom de Telecomix était déjà dans les médias (à cause d’Hosni Moubarak qui avait coupé les intertubes en Égypte), et j’aidais déjà pour le projet [Streisand](https://streisand.okhin.fr/).

Je pense qu’on n’entre pas dans Telecomix. Ce n’est pas un endroit, principalement parce qu’il est possible de sortir d’un endroit, donc on ne peut pas y entrer. Vous ne pouvez pas vous y inscrire, car il n’y a pas de système d’inscription (quiconque vous dirait le contraire tenterait de vous avoir, mais c’est pas le sujet). Vous évoluez juste en quelque chose qui est Telecomix. Votre état d’esprit change, et évolue en ça.

Donc, vous vous réveillez un jour, et c’est comme « OMG!!!!!! I’M TELECOMIX NAO!!!!! ». Une fois que la caféine est redescendue dans votre organisme, et une fois que vous aurez laissé passer la matinée, vous vous rendrez compte que ces gens ne sont rien de plus ou de moins que des personnes normales.

Il n’y a pas de crypto-anarchistes qui parlent en langues hermétiques, qui engueulent tous ceux qui n’utilisent pas de systèmes cryptographiques forts et des conventions sociales cryptographiques ; il n’y a pas d’IA super-intelligente qui tente de dominer le monde ; il n’y a pas de supra-hackers qui se nourrissent de données et de caféine ; il n’y a personne qui cherche à sauver le monde.

### Entrer dans la Matrice

C’est en partie vrai. Nous avons des bots qui peuvent se montrer schizophrènes et sociopathes à la fois. Il y a beaucoup de personnes différentes et uniques, venant de tout point du cyberspace. Il y a des sociologues, des ingénieurs en informatique, des fainéants, des hackers, des brasseurs de bière, des paranoïaques adeptes des théories conspirationnistes, des gens politisés et des apolitiques, et je suspecte même quelques aliens de participer au cluster.

Certains peuvent se demander à quoi ressemble un jour ordinaire dans un groupe d’hacktivistes. Je n’en sais rien, je peux seulement parler pour moi, et je pense que ça risque d’en surprendre certains. Est-ce que vous avez vu le film [Hackers](http://www.imdb.com/title/tt0113243/) ? Vous devriez, il est sympa. Mais ça ne se passe pas comme ça.

Je passe énormément de temps à simplement être assis devant un ordinateur, à regarder des écrans remplis de terminaux (oui, je prends du plaisir à avoir des ordinateurs que personne d’autre que moi n’est capable de comprendre ou d’utiliser). Je fais ça pour mon travail, et pour mes hobbies.

Si vous regardez au-delà de l’écran, vous verrez que je suis connecté à pas mal de salons de discussion, qui ne racontent pas tant de choses que ça. Même lorsque j’écris des trucs, ou quand je bosse, par exemple sur ce billet pour mon usage personnel, je tape sur une console. En sirotant un café noir, sans me rendre compte qu’il est deux heures du matin, vous pouvez passer énormément de temps à discuter avec les gens, tandis que vous développez des programmes, que vous analysez des infrastructures, ou que vous ondulez simplement au travers des intertubes. C’est ce que je fais toute la journée. Mon job le requiert, j’aime le faire, et je le fais aussi avec les membres de Telecomix.

C’est mon pain quotidien. Me réveiller en retard, passer beaucoup trop de temps sur IRC et les intertubes, passer pas assez de temps à sortir avec des gens, se coucher trop tard. Et aussi me rendre dans des hackerspaces ou des conférences, pour faire des choses, échanger des connaissances et compétences avec les gens en *viande*. Ho, et jouer à pas mal de jeux (jeux de rôle avec crayons et papier, jeux vidéos, etc.), et passer du temps avec les médias quand ils le demandent.

Alors, vous voyez, j’ai une vie plutôt ordinaire. Je ne m’infiltre pas sous couverture dans les bases secrètes pour voler un ordinateur, je ne pirate pas des systèmes gouvernementaux juste pour trouver votre numéro de carte de crédit. J’essaie simplement de trouver de nouvelles façons de faire [circuler les données](https://datalove.me/), parce que c’est ce qui m’intéresse.

### Rencontrer le cluster

Demander à un agent ce qu’est Telecomix vous plongera dans un abîme de perplexité, car aucun de nous n’en a la même définition. Nous nous posons nous-mêmes souvent cette question, et la réponse change invariablement, sans que nous trouvions un consensus (mais nous n’en cherchons pas).

Il est clair que nous ne sommes pas une organisation, dans le sens où nous n’avons pas de tête identifiée, d’agenda, de plans ou de fonds. Nous pensons être un cluster encore trop centré. Pourquoi ça ? Parce que les gens se fient à nous au lieu d’essayer de construire leurs propres trucs. En tout cas, c’est l’impression que j’en ai de l’intérieur.

Nous pourrions faire bien plus de choses si nous avions des jours de 35 heures, ou si nous avions un moyen de travailler à temps plein pour Telecomix. Mais alors, je pense qu’on perdrait beaucoup de fun. Et c’est le plus important dans Telecomix : le fun. Nous sommes là pour passer du bon temps, faire des choses qui nous plaisent, des choses qui sont importantes (comme décentraliser la planète), mais on ne peut faire ça à ce rythme que si on a l’opportunité de rire et d’y prendre du plaisir.

C’est la partie qui peut décontenancer les gens. Nous ne changeons pas le monde parce que nous le devons. Merde, qui sommes-nous pour penser que nous devons changer le monde ? Le seul au monde qui puisse le faire, c’est vous. Nous changeons le monde parce que c’est marrant. Les trucs les plus dingues que nous ayons faits, nous les avons faits parce que nous nous sommes amusés à les faire.

Je me suis amusé à travailler sur des VPNs et des darknets fournis aux Syriens. Je n’ai pas fait ça parce que quelqu’un devait le faire. Ce n’est pas mon combat, et cette révolution appartient aux Syriens. Je l’ai fait parce que je voulais apprendre à ce sujet, je voulais tester comment les communications réseau pouvaient fonctionner dans des conditions difficiles. Lorsque le réseau a été attaqué par Hosni Moubarak, le cluster a simplement testé si on pouvait travailler avec les vieilles lignes analogiques, et comment les diffuser.

Nous nous amusons simplement avec des des situations bizarres et inattendues, parce que si nous faisions ça en pensant que nous le devions et que nous étions les seuls à pouvoir le faire, nous nous cramerions le cerveau.

### La leçon la plus difficile

C’est difficile à comprendre. Lorsqu’on travaille avec un groupe de personnes au sein duquel il y a toujours du monde connecté qui discute de choses intéressantes, lorsqu’on aide des personnes par-delà le monde à essayer de communiquer et qu’elles se font arrêter et probablement tuer pour l’avoir fait, on prend un sérieux coup sur le moral. La caféine et le stress ne font pas bon ménage, et si vous y ajoutez un manque de sommeil, ça va vite devenir critique.

La force d’un cluster est sa redondance. Travailler avec autant de personnes différentes, sur autant de sujets différents (des radios amateurs aux darknets, en passant par les drones et ACTA) donne la possibilité de simplement partir et se déconnecter.

Ça ne vous fait pas plaisir, surtout lorsqu’il y a des vies en jeu. Mais vous ne serez plus bon à rien après 36h de veille, saturé de caféine, d’alcool, et de Cameron sait quoi. Vous devez maintenir une vie hors du cluster, ou vous devriendrez un bot.

La force d’un petit groupe d’hacktivistes (nous sommes 220 connectés sur #telecomix au moment où j’écris ça) est la différence de ses membres. Nous sommes souvent en désaccord sur pas mal de sujets, mais ce n’est pas un problème, nous sommes dans une faisocratie et si je veux que quelque chose soit fait, il me suffit de le faire. Et nous avons beaucoup à apprendre des gens qui nous sont différents.

Vivre avec des gens qui partagent votre idéal et toutes vos opinions est ennuyeux. Nous avons vécu des crises, et nous en connaîtront d’autres, parce que c’est comme ça qu’un système chaotique et non planifié devrait grandir.

### Exécuter

Nous n’avons pas de plans. Pas de programme. Nous avons quelques canaux restreints qui existent principalement pour des raisons techniques. Ces raisons incluent le fait de crier votre rage au sujet de quelqu’un, en espérant que quelqu’un sera d’accord avec vous, vous rendre compte que vous êtes tout seul et que vous êtes un connard et un imbécile, puis vous calmer, vous rappeler de la commande /ignore, et revenir à la normale en marmonnant quelque chose à propos du retour de Cthulhu ou un truc du genre.

Le fait est que je perçois Telecomix comme une idée. Une puissante, en perpétuel changement. Ou comme un bar virtuel, dans lequel vous aurez des verres gratuits, servis par de beaux serveurs, serveuses et poulpes, tous virtuels. Mais vous avez saisi le principe. Ou pas. Je m’en fous.

Je ne suis pas sûr d’être arrivé à quoi que ce soit avec ça, mais je sais que j’ai pris du plaisir à l’écrire. Ça me fait me demander si vous aurez du plaisir à le lire. Je suis pas sûr que ça ait du sens.

Alors lançons ce *git push*, on verra bien ce que ça donne.
