Title: NON à ACTA
Permalink: non-a-acta
Date: 2011-10-29
Modified: 2011-10-29
Tags: ACTA, LQDN
Category: Hacktivisme

Un très court billet pour relayer l’excellente vidéo réalisée par [la Quadrature du Net](https://www.laquadrature.net/). Comme le dit si bien le camarade [Benjamin](https://status.ndn.cx/notice/242518), elle est juste parfaite pour expliquer le danger incroyable que représente cet accord secret et anti-démocratique, négocié en ce moment-même. Je vous invite chaudement à la relayer, à vous renseigner sur ACTA et à faire votre devoir de citoyen en appelant vos élus à refuser un tel déni de démocratie.

<!-- cut -->

<div class="center">
<video src="/files/lqdn-non-acta-big.webm" style="width: 80%;" controls><source src="/files/lqdn-non-acta-big.webm" type="video/webm" /></video>
</div>
