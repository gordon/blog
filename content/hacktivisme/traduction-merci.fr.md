Title: [Traduction] Merci
Permalink: traduction-merci
Date: 2012-02-24
Modified: 2012-02-24
Tags: okhin, syrie, telecomix, traduction
Category: Hacktivisme

*Encore une traduction de l’ami [Okhin](https://about.okhin.fr/posts/Thank_You/). Parce que son billet est émouvant, qu’il rappelle qu’il y a un cœur derrière l’IP. Merci à lui, et surtout, merci à ceux qui risquent leur vie quotidiennement en Syrie.*

<!-- cut -->

### Merci

J’ai quelque chose en tête, quelque chose que je ne peux m’empêcher de poser par écrit. Je me sens très mal à l’aise à ce propos, et ça me rend presque malade. Ouais, ça m’arrive parfois, et ça veut dire que je ne suis pas un sociopathe fini.

C’est un problème en rapport avec les journalistes, reporters, et avec tous ceux qui font leur possible pour rapporter des nouvelles. Je n’ai de problème avec aucun d’eux, et ils font pour la plupart un boulot incroyable.

Ils risquent leur vie quotidiennement en Syrie. Aujourd’hui *(le 22 février, NdT)*, ce sont deux d’entre eux qui ont été tués après avoir retransmis un live depuis Homs, et ils étaient probablement bons pour faire leur travail. Hier, c’est un reporter citoyen qui a également été tué, et il ne s’agit là que des nouvelles du front qui nous parviennent.

Mon problème est à propos du « Que peut-on faire ». Avec le cluster Telecomix et les volontaires d’OpSyria, nous sommes, pour la plupart, assis dans nos bureaux, à parler avec les médias ou d’autres trucs du genre. On essaie de toujours prendre du plaisir à le faire, parce que sinon, nous serions incapables de gérer tout ça, mais nous n’avons jamais mis les pieds au front.

Nous avons des contacts là-bas, dont certains ont disparu. C’est grâce à eux que nous pouvons alimenter nos [sites](http://syria.telecomix.org/) d’informations, mais nous ne mettons pas nos vies en danger (yeah, on a fini par comprendre que la vie était un jeu vidéo avec un seul crédit).

Parfois, des journalistes viennent sur nos chans IRC pour nous demander des conseils. Ils nous demandent s’ils peuvent aller en Syrie. Et nous ne savons pas quoi répondre.

Soit nous épargnons leur vie, celle des arrangeurs qu’ils auront là-bas et celle des gens qu’ils rencontreront, mais alors nous jouons le jeu d’Assad : encourager le blackout des informations du front ; soit nous leur rappelons simplement de rester à l’abri, d’utiliser un chiffrement fort, de ne pas avoir de notes ou d’éléments qui pourraient identifier les gens.

Mais tous ces conseils sont bons du moment que vous ne vous trouvez pas dans une ville assiégée jour et nuit depuis des semaines. Et on voit des gens mourir chaque jour, en tentant d’obtenir des témoignages et de faire leur job. Nous sommes juste des archivistes, nous tentons de mettre en perspective les données récoltées au jour le jour, mais sans ces gens incroyables sur le terrain (qu’ils soient citoyens, journalistes, ou envoyés spéciaux internationaux), nous ne serions pas capables de faire tout ça.

La semaine dernière, j’étais à une conférence pour discuter de l’interaction entre les hackers et les ONG, quand quelqu’un m’a demandé :

> Quels sont vos plans pour la Syrie maintenant ?

Je sais pas. J’en ai pas la moindre foutue idée. Nous maintenons nos systèmes de communications, mais quand vous êtes sous les bombes, sans électricité, nourriture ou eau durant plusieurs jours, ça ne sert à rien. J’ai pas le moindre putain d’indice sur ce que nous pouvons faire. Nous se sommes pas des agents de terrain.

Je ne vois aucun espoir de résolution pacifique, et maintenant que les forces d’Assad ont reçu l’ordre d’assassiner des [journalistes](http://www.telegraph.co.uk/news/worldnews/middleeast/syria/9098511/Marie-Colvin-killed-Syrian-forces-had-pledged-to-kill-any-journalist-who-set-foot-on-Syrian-soil.html), je ne vois même pas comment ça pourrait être possible.

Je ne sais pas quoi dire. Les journalistes doivent y aller, c’est impératif pour savoir ce qui se passe là-bas, mais ils se feront assassiner.

Je me soulèverai pour la liberté en Syrie. Nous autres, en tant qu’humains, avons besoin de savoir quelle est la situation là-bas, pas par voyeurisme macabre, mais pour pouvoir être témoins, et apporter toute aide possible.

Alors, à tous ces gens qui mettent leurs vies en jeu pour rapporter des informations sur la Syrie, je veux dire Merci. Vous n’êtes pas seuls, vous ne serez pas oubliés. Continuez votre boulot formidable. Envoyez des rapports. Tentez de rester raisonnablement en sécurité, bien que ça n’ait pas de sens sur le champ de bataille. La violence ne doit pas tuer l’information. Si vous avez besoin de la moindre aide pour cacher vos communications ou pour en établir de plus ou moins sûres, venez discuter avec nous.

Et à tous les rédacteurs ici-bas, ou tous les éditeurs qui parfois suppriment ces contenus des Intertubes, on vous surveille. Vous savez ce qui se passe là-bas. Vous devez en parler.

Merci. Vraiment.

**Rajout** : [The Express](http://www.lexpress.fr/diaporama/diapo-photo/actualite/monde/avant-remi-ochlik-et-marie-colvin-les-journalistes-et-blogueurs-morts-en-syrie_1085370.html) a établi une liste, probablement non exhaustive, des reporters morts en Syrie.
