Title: [Flashback]Howto pratique : l’installation complète d’une Gentoo Linux
Permalink: howto-installation-gentoo-linux
Date: 2011-09-27
Modified: 2011-09-27
Tags: gentoo, howto, luks, cryptographie
Category: Sysadmin

Et quand je dis « une Gentoo », c’est pour faire référence à sa qualité de « meta-distribution ». Car il n’y a pas une seule Gentoo, mais des milliers différentes, chacune optimisée pour un usage précis. Mais débutons par quelques explications :

### WTF is Gentoo ?

[Gentoo Linux](http://gentoo.org) est un système d’exploitation libre, comme peut l’être [Debian](http://debian.org) ou [FreeBSD](http://freebsd.org). Les néophytes, auxquels cet article ne s’adresse absolument pas, en parleront sans doute sous l’amusant sobriquet « Linux ». La principale spécificité de Gentoo est qu’elle est distribuée sous forme de sources uniquement (à une petite poignée d’exceptions près). Ce qui a l’immense avantage d’être extrêmement flexible et modulaire (à l’image du noyau Linux même). Là où Ubuntu s’installe sans trop râler en cliquant frénétiquement sur « Suivant », l’apprenti Gentooiste mettra les mains dans le cambouis et « construira » lui-même son système, pièce par pièce, et en compilant lui-même et avec ses petites options gcc (ou autre, d’ailleurs).

Mais qui dit avantage, dit inconvénient aussi. Sauf les [fanboys d’Apple](http://www.gordontesos.com/telephonie-et-smartphone/317-je-hais-apple.html), mais eux sont irrécupérables. L’inconvénient majeur donc, est également l’avantage cité plus haut : faut y mettre les mains. Faut passer du temps pour obtenir un système fonctionnel, mais bon sang de bois, quand c’est le cas, ça tourne bien. Vu que tout aura été compilé en local, les binaires seront optimisés à volonté, et on n’a pas des mainteneurs qui foutent la merde dans les paquets, comme sous [Kubuntu](http://kubuntu.org), dont l’intégration de KDE est bugguée à souhait. C’est donc la raison d’être de ce billet, qui s’annonce particulièrement long : vulgariser l’installation de Gentoo, celui-ci n’ayant pas vocation de remplacer l’[excellente documentation](http://gentoo.org/doc/fr/handbook/) indispensable à tout Gentooiste. Et pour faire dans l’inédit, j’écrirai ce document au cours de ma propre installation d’une nouvelle machine.

<!-- cut -->

### Ce qui sera expliqué ici

Toutes les étapes du Handbook seront commentées, avec les difficultés rencontrées par mes soins, mes choix et leur explication par rapport à certains éléments, et autres…

Mon installation a pour objectif de monter une machine de bureau, avec l’accent mis sur la sécurité (le disque dur sera intégralement chiffré), et les performances (histoire d’utiliser au mieux le matos de guerre que je me suis offert).

En parlant de matériel, voici la configuration de référence de ce document :

* Processeur Intel i7 930 (4 cœurs multi-threads, ce qui donne 8 processeurs utilisables par le système)
* Carte mère Gigabyte X58A UD3R (chipset Intel X58, chipset audio Realtek ALC889A, contrôleur réseau Realtek RTL8111D)
* 3 barrettes KINGSTON DDR3 PC3-14400 (2Go chacune)
* Carte graphique nVidia Geforce GT240 (1024Mo de RAM DDR3)
* SSD Crucial RealSSD C300 de 64Go

Le reste ayant peu d’importance, on peut d’ores et déjà prendre en compte ce matériel dans la conception du système :

* le processeur tourne comme un cheval dopé, et dispose de 8 threads, ce qui est très appréciable pour la compilation
* on a 6Go de RAM, ce qui permet d’utiliser du tmpfs (système de fichier monté en RAM, très pratique pour des données volatiles, comme les fichiers temporaires ou fichiers de compilation)
* on a un SSD très rapide et d’une taille assez faible, bien que ça ne soit pas critique au point de chercher l’optimisation de la taille des binaires. Il faudra le prendre en compte pour tenter d’optimiser son utilisation.

### Ce qui ne sera pas expliqué ici

Comment installer Gentoo Linux. Ça paraît con, mais il existe une doc officielle, très complète, et qui constitue une référence. Je ne suis pas là pour la remplacer, seulement pour présenter à vif ma propre installation.

### A qui s’adresse ce walloftext indigeste ?

À ceux qui auront compris au moins 90% de ce que je viens d’écrire. Ou qui ont beaucoup de temps à perdre.

### Let’s go Baby!

Malheureusement, je n’ai pas eu la présence d’esprit de mitrailler frénétiquement de photos le déballage et le montage des pièces de la machine, donc je vais religieusement passer sous silence cette étape, qui a eu pour principale difficulté un chaton qui sautait de partout et essayait de se frotter à la carte mère. Une fois la tour assemblée, je me suis mis à la recherche d’une distribution GNU/Linux live en 64 bits, qui servirait d’hôte pour l’installation manuelle de Gentoo. Premier problème, le lecteur/graveur de mon pc portable (jusque là ma machine principale) est en rade (ou plus reconnu par le système, je n’ai pas pris le temps de me pencher dessus). Donc impossible de graver un ISO. J’ai tenté de télécharger 3 ou 4 distributions, que j’ai copiées sur une clé USB (via [dd](http://www.linux-kheops.com/doc/man/manfr/man-html-0.9/man1/dd.1.html) ou [Unetbootin](http://unetbootin.sourceforge.net/)), sans succès : soit il s’agissait d’un ISO non prévu pour booter sur USB (avec une mauvaise config d’isolinux), soit c’est Unetbootin qui foirait joyeusement la copie… J’ai enfin réussi à démarrer correctement sous une [OpenSuse](http://www.opensuse.org/) 11.3 64bits (à savoir que c’est une distrib dont l’iso passe très bien sur une clé USB). Le matériel était bien reconnu (après avoir du passer le SSD en AHCI dans le BIOS). J’en profite pour préciser que si la sortie son ne fonctionne à priori pas, l’augmentation du volume des haut-parleurs peut être une solution viable.

Une fois le système live démarré, donc, on va s’en servir comme hôte pour construire la Gentoo. Et pour plus de facilités, et étant donné que j’ai un boulot, on va s’autoriser de travailler à distance (ce qui n’impacte pas la productivité, car il s’agit d’opérations simples mais longues, qui se font en arrière-plan). Pour cela, SSH est tout indiqué; on aura néanmoins le soin de fixer un mot de passe sur l’user root, ainsi que sur « linux », l’utilisateur du live-cd, grâce à la commande passwd. Ensuite, un « /etc/init.d/sshd start » pour lancer le démon SSH, et on peut se connecter. Si la machine est derrière un routeur, ce qui est le cas de la mienne, il est pratique d’utiliser l’IPV6 pour s’y connecter (en espérant que le FAI le permette). Sinon, un lien VPN est possible, ou alors il faut ouvrir le port 22 sur le routeur. L’opération n’entrant pas dans le cadre de l’installation, et ne me concernant de toutes façons pas, elle sera à votre discrétion.

Enfin, par sécurité, on installera le paquet « screen » sur le système hôte. Celui-ci permettra de ne pas couper les processus en cours dans le terminal si celui-ci, ou la connexion, tombe. Avant de lancer l’install, on tape « screen » pour entrer dans un screen. Je vous laisse lire la doc du lien ci-dessus pour savoir comment récupérer un screen si besoin.

Ha, et avant que j’oublie, le Handbook EN est plus récent, et en général plus fiable, que sa traduction française, pourtant de qualité. Donc, maintenant (si vous suivez toujours), suivez le guide officiel, mes commentaires appuieront certains détails.

### 1 — Préparation du disque

Comme précisé auparavant, je souhaite optimiser l’utilisation de mon SSD au maximum. C’est à dire lui éviter au maximum les opérations d’écriture trop lourdes. On peut donc distinguer 2 gros dossiers qu’il serait préférable de ne pas placer sur le SSD : /tmp et /var/tmp/portage. Il est tout à fait intéressant de les monter en RAM via [tmpfs](http://www.gentoofr.org/tmpfs.html). Je songe à l’achat d’un disque dur rapide en complément du SSD pour y placer d’autres FS (comme les fichiers medias, qui seraient trop à l’étroit sur le SSD), mais en attendant, ils seront montés sur un disque dur externe USB.

Rappelons au passage que le disque sera intégralement chiffré (même pour les partitions sur le disque mécanique). C’est un élément à prendre en compte à ce stade. La première chose à faire, selon ce [guide](http://en.gentoo-wiki.com/wiki/DM-Crypt_with_LUKS), est donc d’installer les paquets suivants sur le système hôte (je donne leur nom dans l’arbre Portage, puis, comme j’utilise OpenSuse, leur nom sous ce système) :

* sys-fs/cryptsetup (cryptsetup sous OpenSuse, est déjà installé dans le live-cd)
* app-crypt/luks-tools (ça a l’air déjà installé dans la OpenSuse)

Tout d’abord, nous créons les mappings chiffrés avec LUKS : je me baserai sur le guide donné plus haut, dans l’optique de chiffrer toutes les partitions, sauf /boot qui contiendra le script de décryptage (mais qui demandera tout de même d’entrer une passphrase). Le swap est également chiffré.

Il convient évidemment de faire **très** attention à sa passphrase, et éventuellement à sa clé, selon la méthode employée (une copie chiffrée avec une autre clé, stockée en un ou plusieurs endroits, disons). J’ai utilisé l’algo serpent en 256 bits, essentiellement parce que je trouvais le nom cool (mon petit disque ne contiendra vraisemblablement pas de documents capables de mettre en péril la sécurité nationale). Dans le doute, j’ai utilisé la même clé pour les 3 mappings (root, home et swap).

Au moment de configurer mon kernel, j’ai posé une question à ce sujet sur IRC, et on m’a fait comprendre que la méthode de chiffrement par GPG n’était pas une très bonne idée : en effet, la faiblesse se situe toujours sur la passphrase, étant donné que la clé est stockée sur le disque. De plus, on cumule les éventuelles vulnérabilités de GPG ET de Luks. Ainsi, j’ai préféré opter pour la méthode de la passphrase seule,. La procédure pour changer de passphrase ou de clé est heureusement très simple, n’endommage pas les données, et ne nécessite même pas de démonter les volumes :

    :::console
    # gpg --decrypt /mnt/gentoo/boot/root-key.gpg 2>/dev/null # on tape la passphrase existante, puis on copie la clé renvoyée
    # cryptsetup luksAddKey /dev/sda1 # on n'entre pas le mapping, mais bien le périphérique
    Enter any LUKS passphrase: # c'est pour ça qu'on a copié la clé précédemment (si on l'avait pipée, cryptsetup ne nous aurait pas demandé de nouveau mot de passe)
    key slot 0 unlocked.
    Enter new passphrase for key slot: # c'est là qu'on entre le nouveau mot de passe
    Verify passphrase: # on le vérifie
    Command successful. # à ce stade, il sera possible de déchiffrer le disque avec les 2 clés (celle aléatoire chiffrée par GPG, et la nouvelle). Ce qui est une excellente chose pour éviter de perdre les données, mais dans notre cas, on ne veut qu'une clé. Il suffit de supprimer l'ancienne
    # cryptsetup luksDelKey /dev/sda1 0 # le 0 correspondant au slot de la clé. Un cryptsetup luksDump <device> nous montre les slots utilisés.

Voici pour mémoire les commandes à utiliser pour créer, ouvrir et monter une partition cryptée :

    :::console
    # gpg --quiet --decrypt root-key.gpg | cryptsetup -v --cipher serpent-cbc-essiv:sha256 --key-size 256 luksFormat /dev/sda # nous formatons en format "Luks" la partition /dev/sda1, avec la clé "root-key.gpg" préalablement générée (voir le guide)
    # gpg --decrypt root-key.gpg 2>/dev/null | cryptsetup luksOpen /dev/sda1 root # on crée le mapping lié à cette partition, avec la même clé. /dev/mapper/root sera donc notre partition utilisable.
    # cryptsetup luksClose /dev/mapper/root # on ferme le mapping

Le guide nous invite agréablement à poursuivre l’installation pour les phases 5 à 7.

Maintenant qu’on a les mappings, on procède au partitionnement en les utilisant. Voici donc le schéma choisi pour mon système. /dev/sda représente le disque SSD et /dev/sdb le second disque :

<dl>
<dt>/ (/dev/mapper/root => /dev/sda1) : ext4 (options: noatime, discard, errors=remount-ro, nobarrier, commit=50) 45Go</dt>
<dd>Il s’agit d’options piochées ici et là pour optimiser les opérations d’écriture sur le disque : l’ext4 est un choix qui me semble sûr à cause de la maturité du FS, qui reste plutôt simple, et de son support du TRIM des SSD (j’ignore si d’autres le prennent en compte depuis). J’aurais bien aimé pouvoir profiter du BTRFS, mais celui-ci n’est toujours pas stable. Vu la taille du SSD (64Go), attribuer 45Go me semble correct.</dd>
<dt>/boot (/dev/sda2) : ext2 (options : noatime, noauto) 4Go</dt>
<dd>La partition /boot (contenant les noyaux, la configuration de grub, etc) n’a pas besoin d’être montée, ni d’être journalisée : elle ne sera utilisée par le système que lors de l’installation d’un nouveau noyau. Sa taille lui permet de contenir confortablement un certain nombre de kernels.</dd>
<dt>/home (/dev/mapper/home => /dev/sda3) : ext4 (mêmes options que pour /) 15Go</dt>
<dd>On se donne un petit /home confortable, sachant que les fichiers medias seront stockées ultérieurement sur une partition de sdb. Les options choisies pour / conviennent tout à fait.</dd>
<dt>/tmp : tmpfs (options: defaults, nosuid, nodev, noexec, noatime) 1Go</dd>
<dd>On donne 1Go à /tmp, ce qui est largement suffisant. Pour des raisons de sécurité, on supprime toute possibilité d’exécution dessus.</dd>
<dt>/var/tmp/portage : tmpfs (options: uid=250, gid=250, mode=775, noatime) 5Go</dt>
<dd>On donne le maximum de RAM possible à /var/tmp/portage, car les fichiers de compilation y seront, et certains packages consomment beaucoup (OpenOffice.org, GCC…). On « donne » ce point de montage à l’user portage</dd>
<dt>SWAP (/dev/mapper/swap => /dev/sdb1) 10Go</dt>
<dd>10Go de swap (mémoire virtuelle), j’aurais pu en mettre 12, mais honnêtement, je n’ai fait que récupérer une vieille partition sur mon disque externe :) promis, j’en mettrai 12 sur le vrai DD</dd>
</dl>

Voici, pour info, les commandes de montage effectuées :

    :::console
    # mkdir /mnt/gentoo
    # mount -o noatime,discard,errors=remount-ro,nobarrier,commit=50 /dev/mapper/root /mnt/gentoo/
    # mkdir /mnt/gentoo/boot
    # mkdir /mnt/gentoo/home
    # mkdir /mnt/gentoo/tmp
    # mkdir /mnt/gentoo/var
    # mkdir /mnt/gentoo/var/tmp
    # mkdir /mnt/gentoo/var/tmp/portage
    # mount -o noatime,noauto /dev/sda2 /mnt/gentoo/boot/
    # mount -o noatime,discard,errors=remount-ro,nobarrier,commit=50 /dev/mapper/home /mnt/gentoo/home
    # mount -t tmpfs -o defaults,nosuid,nodev,noexec,noatime,size=1G none /mnt/gentoo/tmp/
    # mount -t tmpfs -o defaults,uid=250,gid=250,mode=775,noatime,size=5G none /mnt/gentoo/var/tmp/portage/

### 2 – Construction de l’arborescence

Procédons à la récupération des médias d’installation. Personnellement, j’ai eu l’occasion de tester, presque par hasard, [Funtoo](http://funtoo.org), et je dois bien avouer que c’est intéressant : tout d’abord, l’archive stage3, qui permet l’installation, est généralement plus à jour, et surtout, chose très agréable, la synchronisation de l’arbre Portage se fait via git (donc plus rapide, et il ne retransfère pas tout si l’arbre est à jour). Utilisons donc cette archive (note : les archives Funtoo étant en format .xz, il faut s’assurer que le système hôte puisse lire ce format, et au besoin installer les paquets manquants) :

    :::console
    # cd /mnt/gentoo
    # wget ftp://ftp.nluug.nl/pub/metalab/distributions/funtoo/funtoo/amd64/stage3-amd64-current.tar.xz # ce lien est évidemment à adapter selon votre localisation et vos préférences
    # tar xvJpf stage3-*.tar.xz
    # wget ftp://ftp.nluug.nl/pub/metalab/distributions/funtoo/funtoo/snapshots/portage-current.tar.xz
    # tar xvJpf portage-*.tar.xz -C ./usr

On se retrouve donc avec l’architecture initiale de notre système Gentoo ! Quel bonheur ! La suite est encore plus marrante.

### 3 — Configuration de la compilation

Cette étape est primordiale, car elle définit les optimisations du système entier. Elle définit également l’usage précis qu’on fera de ce système, par le biais des [USE flags](http://www.gentoo.org/doc/en/handbook/handbook-amd64.xml?part=2&chap=2) (mots-clés définissant les options à activer pour chaque programme : si nous ne souhaitons pas du support du Bluetooth sur ce pc, inutile de s’encombrer de ces options dans les programmes). Tout cela s’effectue dans le célèbre */etc/make.conf* ! (attention, étant donné que nous ne sommes pas encore dans le chroot, il convient d’éditer */mnt/gentoo/etc/make.conf*)

Il va falloir déterminer les optimisations selon notre processeur : un *cat /proc/cpuinfo* permet de mettre en avant les capacités suivantes :

    :::console
    flags : fpu vme de pse tsc msr pae mce cx8 apic mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe nx rdtscp lm constant_tsc arch_perfmon pebs bts xtopology nonstop_tsc aperfmperf pni dtes64 monitor ds_cpl vmx est tm2 ssse3 cx16 xtpr pdcm sse4_1 sse4_2 popcnt lahf_lm ida tpr_shadow vnmi flexpriority ept vpid

Nous activerons donc dans le make.conf les options reconnues. Tout est expliqué dans [ce guide](http://www.gentoo.org/doc/en/gcc-optimization.xml), et il est intéressant de partir d’un système déjà optimisé. Voici donc le make.conf :

    :::bash
    ACCEPT_KEYWORDS="amd64"
    FEATURES="mini-manifest" # il s'agit d'un ajout de Funtoo, qui permet d'économiser beaucoup de place dans l'arbre Portage. On apréciera donc cette fonction
    CHOST="x86_64-pc-linux-gnu"
    CFLAGS="-march=native -O2 -pipe -msse -msse2 -msse3 -mmmx" # le -O2 est un classique, inutile de chercher ailleurs. On active également les flags trouvés plus haut.
    CXXFLAGS="${CFLAGS}"
    MAKEOPTS="-j9" # W00T ! 9 processus de compilation, car on a un processeur qui casse de la belette en buvant le thé !

Nous reviendrons plus tard sur les USE flags à utiliser. Passons sans plus tarder à l’installation en elle-même du système :) 

### 4 — Installation

Utilisant Funtoo, et donc un arbre Portage basé sur git, il est inutile de spécifier le miroir comme indiqué dans le handbook. N’oubliez pas de copier le fichier */etc/resolv.conf*, nous en aurons bien besoin dans le chroot. N’oublions pas non plus de monter les répertoires système :

    :::console
    # mount -t proc none /mnt/gentoo/proc
    # mount -o bind /dev /mnt/gentoo/dev

Et c’est parti pour le Chroot !! Entrons ensemble les commandes suivantes :

    :::console
    # chroot /mnt/gentoo/ /bin/bash
    # env-update
    # source /etc/profile
    # export PS1="(chroot) $PS1"

PS : si la commande chroot renvoie une erreur, vérifiez que l’archive stage3 a bien été décompressée (donc que */mnt/gentoo/bin/bash* existe), et surtout que votre live cd est bien dans la même architecture que notre nouveau système (ça m’est arrivé, j’étais sur un live-cd 32 bits)

Et voilà ! Comme le dit si bien le handbook, nous sommes dès à présent dans notre Gentoo ! Mais elle va avoir du mal à démarrer seule pour l’instant… Un petit *emerge --sync* nous mettra l’arbre Portage à jour. Si une erreur survient, vérifiez que vous avez copié le fichier */etc/resolv.conf*, car c’est lui qui contient les noms des serveurs DNS à utiliser.

Attention, pour les Funtoo users (donc moi), il convient d’effectuer une opération avant le sync :

    :::console
    # cd /usr/portage
    # git checkout funtoo.org

Nous risquons d’avoir de jolis messages colorés au terme du sync. Ne prenons pas une sale habitude d’Ubuntero en les ignorant :

    :::console
    * IMPORTANT: 1 config files in '/etc' need updating.
    * See the CONFIGURATION FILES section of the emerge
    * man page to learn how to update config files.
    
    * IMPORTANT: 1 news items need reading for repository 'funtoo'.
    * Use eselect news to read news items.

Le premier signifie qu’il faut mettre à jour un fichier de configuration (de supers outils existent pour cela), et le second indique l’arrivée d’une news importante liée à Portage. Occupons-nous tout d’abord de cette dernière :

    :::console
    # eselect news read

Elles peuvent indiquer qu’un paramètre par défaut a changé, ou l’arrivée prochaine d’une version majeure du compilateur GCC, etc… Elles indiquent de plus si une procédure particulière devra être appliquée. Dans mon cas, il s’agissait d’un changement de flag par défaut dans la variable d’environnement LDFLAGS. Un truc cool, quoi. Je vérifie rapidement que je n’ai pas par hasard déclaré cette variable dans mon make.conf en écrasant ses valeurs par défaut. Ce n’est pas le cas, parfait, on passe à la suite.

Il existe à ma connaissance 2 outils pour gérer les modifications de fichiers de configuration, suite à une mise à jour. Il faut savoir qu’une mise à jour peut apporter des modifications de syntaxe, ou plus probablement de nouvelles options. Lorsqu’on met à jour un paquet, on se retrouve donc confronté à ce « problème » : dois-je écraser mon fichier existant avec celui fourni ? Dois-je au contraire garder le mien et rejeter en bloc les modifications ? est-il préférable de merger dynamiquement les fichiers pour garder ma configuration perso ? Les commandes « dispatch-conf » et « etc-update » vous permettront de faire tout ça ! Historiquement, j’ai toujours utilisé etc-update, mais il paraît que dispatch-conf est plus complet dans certains points. On va utiliser celui-là, pour rigoler :

    :::console
    # dispatch-conf
    --- /etc/locale.gen 2010-09-12 08:46:57.000000000 +0000
    +++ /etc/._cfg0000_locale.gen 2010-09-12 09:06:00.000000000 +0000
    @@ -15,5 +15,17 @@
    # rebuilt for you. After updating this file, you can simply run `locale-gen`
    # yourself instead of re-emerging glibc.
    -en_US ISO-8859-1
    -en_US.UTF-8 UTF-8
    +#en_US ISO-8859-1
    +#en_US.UTF-8 UTF-8
    +#ja_JP.EUC-JP EUC-JP
    +#ja_JP.UTF-8 UTF-8
    +#ja_JP EUC-JP
    +#en_HK ISO-8859-1
    +#en_PH ISO-8859-1
    +#de_DE ISO-8859-1
    +#de_DE@euro ISO-8859-15
    +#es_MX ISO-8859-1
    +#fa_IR UTF-8
    +#fr_FR ISO-8859-1
    +#fr_FR@euro ISO-8859-15
    +#it_IT ISO-8859-1
    >> (1 of 1) -- /etc/locale.gen
    >> q quit, h help, n next, e edit-new, z zap-new, u use-new
    m merge, t toggle-merge, l look-merge:

Il s’agit donc du fichier */etc/locale.gen*. D’expérience, je sais qu’il s’agit du fichier recensant les locales activées pour le système. Notons que nous n’avons pas encore modifié ce fichier dans la procédure normale d’installation. Notons également que les différences entre les fichiers sont proprement indiquées avec des « + » ou « - » en début de ligne. Nous reviendrons sur ce fichier, donc pour l’instant, acceptons les modifications par un subtil appui sur la touche « u ». Notre arbre est à jour, nous pouvons continuer l’installation.

Juste une chose, si à la fin du sync, vous avez eu un message indiquant qu’une nouvelle mise à jour de Portage était disponible, il vaut mieux l’installer immédiatement : tapez *emerge --oneshot portage* (emerge étant la commande magique pour utiliser Portage, et fournie dans ce dernier). Vous voyez, ce n’est pas plus compliqué que sous n’importe quel GNU/Linux d’installer un paquet sous Gentoo ! (c’est juste un peu plus long ;) )

La phase suivante nous introduit aux USE flags : il s’agit de la sélection du profil, qui définit grossièrement quelle utilisation on fera de cette machine en activant par défaut certains USE. Tapez *eselect profile list* pour en avoir un aperçu :

    :::console
    Available profile symlink targets:
    [1] default/linux/amd64/2008.0 *
    [2] default/linux/amd64/2008.0/desktop
    [3] default/linux/amd64/2008.0/developer
    [4] default/linux/amd64/2008.0/server

On a, pour cette architecture et sur Funtoo, 4 profils disponibles (le profil actif par défaut étant le premier, comme indiqué par « *« . Il ne faut pas se leurrer du nom « developer », celui-ci ne s’adresse qu’aux développeurs Gentoo, et non pas aux utilisateurs souhaitant un environnement de développement. Le profil « desktop » semble le plus approprié pour nous, activons-le avec *eselect profile set 2* (le 2 étant le numéro au début de ligne du profil souhaité).

Occupons-nous maintenant de configurer les USE flags, pour obtenir un système parfaitement personnalisé : tout d’abord, voyons quels USE flags sont activés par défaut :

    :::console
    # emerge --info | grep USE
    USE="X a52 aac acl acpi alac alsa amd64 berkdb bluetooth branding bzip2 cairo cdr cli consolekit cracklib crypt cups cxx dbus dri dts dvd dvdr dvdread emboss encode esd exif fam firefox flac fortran gdbm gif gpm gtk hal iconv ipv6 jpeg lame lcms ldap libnotify mad mikmod mmx mng modules mp3 mp4 mpeg mudflap multilib ncurses nls nptl nptlonly ogg opengl openmp pam pango pcre pdf perl png ppds pppd python qt3 qt3support qt4 readline reflection sdl session spell sse sse2 ssl startup-notification svg sysfs tcpd tiff truetype unicode usb vorbis wavpack x264 xcb xml xorg xulrunner xv xvid zlib"
    ALSA_CARDS="ali5451 als4000 atiixp atiixp-modem bt87x ca0106 cmipci emu10k1x ens1370 ens1371 es1938 es1968 fm801 hda-intel intel8x0 intel8x0m maestro3 trident usb-audio via82xx via82xx-modem ymfpci"
    ALSA_PCM_PLUGINS="adpcm alaw asym copy dmix dshare dsnoop empty extplug file hooks iec958 ioplug ladspa lfloat linear meter mmap_emul mulaw multi null plug rate route share shm softvol"
    APACHE2_MODULES="actions alias auth_basic authn_alias authn_anon authn_dbm authn_default authn_file authz_dbm authz_default authz_groupfile authz_host authz_owner authz_user autoindex cache cgi cgid dav dav_fs dav_lock deflate dir disk_cache env expires ext_filter file_cache filter headers include info log_config logio mem_cache mime mime_magic negotiation rewrite setenvif speling status unique_id userdir usertrack vhost_alias"
    ELIBC="glibc"
    INPUT_DEVICES="keyboard mouse evdev"
    KERNEL="linux"
    LCD_DEVICES="bayrad cfontz cfontz633 glk hd44780 lb216 lcdm001 mtxorb ncurses text"
    RUBY_TARGETS="ruby18"
    USERLAND="GNU"
    VIDEO_CARDS="fbdev glint intel mach64 mga neomagic nouveau nv r128 radeon savage sis tdfx trident vesa via vmware voodoo"
    XTABLES_ADDONS="quota2 psd pknock lscan length2 ipv4options ipset ipp2p iface geoip fuzzy condition tee tarpit sysrq steal rawnat logmark ipmark dhcpmac delude chaos account"

Attention, il y a plusieurs variables en plus du USE (je les ai mises à la ligne pour faciliter la lecture). Par curiosité, constatons que nous disposons par défaut d’options pour les modules Apache à activer, mais également les INPUT_DEVICES par défaut, ainsi que les VIDEO_CARDS (ces variables nous serviront plus tard). En observant les USE flags, on aperçoit par exemple « X », qui indiquera à certains programmes qu’ils doivent se compiler avec le support de Xorg (dans une utilisation serveur, il sera intéressant de désactiver ce flag, et ainsi d’obtenir la version CLI de ces programmes), « alsa », qui indique qu’on veut ce système de gestion audio, « bluetooth », dont j’ai déjà parlé, « bzip2″ qui active cet algorithme de compression, ou « qt* », qui sont plusieurs variables indiquant qu’on veut compiler le support QT. Si on prévoit d’utiliser un système basé sur GTK comme Gnome, il conviendra, à des fins d’optimisation, de désactiver le support QT, et inversement pour KDE. La description des USE flags est disponible dans ce fichier : */usr/portage/profiles/use.desc*. Pour éditer les USE flags, on modifie le bienveillant fichier */etc/make.conf* : en déclarant la variable USE, on peut ajouter ou retirer des USE flags (ces derniers devant être préfixés de « - »). Il est difficile de savoir quoi choisir la première fois, alors pour ma part, je démarre une autre machine, et je récupère les USE spécifiés, car je sais qu’ils me sont utiles. Voici, après avoir proprement cuisiné ça, le contenu de mon make.conf (je vous préviens, j’ai été gourmand) :

    :::bash
    # These settings were set by the metro build script that automatically built this stage.
    # Please consult /etc/make.conf.example for a more detailed example.
    ACCEPT_KEYWORDS="amd64"
    FEATURES="mini-manifest"
    CHOST="x86_64-pc-linux-gnu"
    CFLAGS="-march=native -O2 -pipe -msse -msse2 -msse3 -mmmx"
    CXXFLAGS="${CFLAGS}"
    MAKEOPTS="-j9"
    VIDEO_CARDS="nvidia"
    INPUT_DEVICES="evdev keyboard mouse"
    LINGUAS="fr"
    # global USE flags
    USE="X xorg openvpn samba hal v4l2 sdl dbus xcb glitz crypt webkit consolekit scanner ppds cairo networkmanager msn skype nsplugin acl acpi avahi bash-completion cups curl curlwrappers cvs dri firefox xulrunner gnutls gzip hal idn imap ipv6 jabber jingle libnotify maildir mbox multilib ncurses nsplugin rss smp ssl subversion syslog threads truetype usb xml zeroconf"
    # USE flags for development
    USE="${USE} php javascript sql mysql mysqli ftp gd iconv imagemagick prce perl ruby posix sqlite sqlite3"
    # multimedia USE flags
    USE="${USE} alsa mp3 vorbis arts mp4 flac jpg gif tiff png dvd cdr aac cdda cddb css dvdr encode exif ffmpeg gimp gstreamer lame mpeg mplayer odbc ogopengl svg theora vorbis"
    # KDE USE flags
    USE="${USE} kde qt4 kontact plasma semantic-desktop"
    # USE flags removal
    USE="${USE} -bluetooth -gnome"

Vous remarquerez que j’ai rajouté les variables dont je parlais plus haut : « VIDEO_CARDS », « INPUT_DEVICES », et « LINGUAS », permettant d’activer le support de ces éléments (qui ne se fait pas via les USE). J’ai également répété pas mal de USE qui étaient déjà par défaut, histoier de les avoir sous le nez explicitement. Occupons-nous maintenant des locales, grâce au fichier cité plus haut : on édite /etc/locale.gen. Voici le mien :

    :::bash
    # /etc/locale.gen: list all of the locales you want to have on your system
    #
    # The format of each line:
    # <locale> <charmap>
    #
    # Where <locale> is a locale located in /usr/share/i18n/locales/ and
    # where <charmap> is a charmap located in /usr/share/i18n/charmaps/.
    #
    # All blank lines and lines starting with # are ignored.
    #
    # For the default list of supported combinations, see the file:
    # /usr/share/i18n/SUPPORTED
    #
    # Whenever glibc is emerged, the locales listed here will be automatically
    # rebuilt for you. After updating this file, you can simply run `locale-gen`
    # yourself instead of re-emerging glibc.
    en_US ISO-8859-1
    en_US.UTF-8 UTF-8
    #ja_JP.EUC-JP EUC-JP
    #ja_JP.UTF-8 UTF-8
    #ja_JP EUC-JP
    #en_HK ISO-8859-1
    #en_PH ISO-8859-1
    #de_DE ISO-8859-1
    #de_DE@euro ISO-8859-15
    #es_MX ISO-8859-1
    #fa_IR UTF-8
    fr_FR ISO-8859-1
    fr_FR@euro ISO-8859-15
    fr_FR.UTF-8 UTF-8
    #it_IT ISO-8859-1

Ensuite, un petit *locale-gen* suffira à regénérer les locales pour notre serveur.

N’oublions pas qu’il faut paramétrer le déchiffrement des disques au boot : on se replonge dans la doc sur LUKS ([celle-ci](http://en.gentoo-wiki.com/wiki/Root_filesystem_over_LVM2,_DM-Crypt_and_RAID) semble plus précise et simple, on m’a conseillé de la suivre en occultant les parties sur le raid et lvm), et on va générer une image initramfs. Il s’agit d’une image système minimaliste chargée en RAM, qui aura pour but de permettre le chargement normal du système. Pour cela, elle utilisera notamment Busybox, qui est un logiciel permettant d’embarquer de façon portable plusieurs logiciels importants, qu’il faut avoir sous la main lorsque le système n’arrive pas à booter, ou pour les opérations à effectuer dès le chargement du kernel ; en l’occurrence, au chargement du kernel, les disques seront chiffrés, il convient de les ouvrir à ce moment.

Je vous laisse suivre le guide pour la création de l’image initram, que vous modifierez avant de la compresser. Celle-ci contient notamment un répertoire dev/, très important car il contient les nodes correspondant aux partitions. Il faut donc vérifier que toutes les partitions qu’on souhaite déchiffrer sont présentes. Si ce n’est pas le cas, il faut créer le node. Pour ceci, un *ls -l /dev/sd** nous donne de précieuses informations :

    :::console
    brw-rw---- 1 root disk 8, 0 22 févr. 09:38 /dev/sda
    brw-rw---- 1 root disk 8, 1 22 févr. 09:38 /dev/sda1
    brw-rw---- 1 root disk 8, 2 22 févr. 09:38 /dev/sda2
    brw-rw---- 1 root disk 8, 16 22 févr. 09:38 /dev/sdb
    brw-rw---- 1 root disk 8, 17 22 févr. 09:38 /dev/sdb1
    brw-rw---- 1 root disk 8, 18 22 févr. 09:38 /dev/sdb2
    brw-rw---- 1 root disk 8, 19 22 févr. 09:38 /dev/sdb3

Les nodes se remarquent par la première lettre de leur chmod : il peut être « p », « b » ou « c ». Référez-vous à *man mknod* pour en savoir plus. De plus, un node se caractérise par deux numéros, qu’on trouve entre la taille du fichier et sa date de création : ici, c’est 8 et 0 à 2 pour le premier disque, et 16 à 19 pour le second. Ma partition sda1 peut donc être « liée » dans l’initramfs par la commande *mknod dev/sda1 c 8 1* (attention, il faut se trouver dans le dossier de l’initramfs pour ça). Ce node peut ensuite être manipulé exactement comme celui dans /dev, c’est à dire qu’on peut le monter, ou lui faire des trucs louches avec Luks. C’est d’ailleurs ce qu’on va faire, en grands sadiques que nous sommes, après nous être assurés que les partitions ont bien été recréées (ici, il nous faudra sda1, sda2, sdb1). Il faut donc créer un « init script », qui sera automatiquement appelé après le chargement du noyau, et qui se chargera de ranger tout ça. Je vous livre le mien tout prêt, étant incapable de retracer les différentes étapes et essais qui m’ont été nécessaires pour parvenir à quelque chose de fonctionnel. Par ailleurs, étant donné que j’utilise la même passphrase pour mes volumes, comme expliqué précédemment, j’ai opté pour une astuce permettant de déverrouiller les trois volumes en une fois. Le fichier à éditer est donc *init* :

    #!/bin/sh
    mount -t proc proc /proc
    CMDLINE=`cat /proc/cmdline`
    mount -t sysfs sysfs /sys
    #wait a little to avoid trailing kernel output
    sleep 3
    #If you don't have a qwerty keyboard, uncomment the next line
    loadkmap < /etc/kmap-fr
    #If you have a msg, show it:
    clear
    cat /etc/msg
    #dm-crypt
    while [ ! -e /dev/mapper/root ]
    do
        read -s -p "Enter passphrase: " lukspass
        echo ""
        echo $lukspass | /bin/cryptsetup luksOpen /dev/sda1 root
    done
    echo "Root opened."
    echo $lukspass | /bin/cryptsetup luksOpen /dev/sda3 home
    echo "Home opened."
    echo $lukspass | /bin/cryptsetup luksOpen /dev/sdb1 swap
    echo "Swap opened."
    unset lukspass
    #lvm
    #/bin/vgscan
    #/bin/vgchange -ay vg
    #root filesystem
    echo "Mounting root filesystem read-only..."
    mount -t ext4 -r /dev/mapper/root /newroot
    #unmount pseudo FS
    umount /sys
    umount /proc
    #root switch
    exec /bin/busybox switch_root /newroot /sbin/init ${CMDLINE}

Comme vous pouvez le voir, il est fait référence au fichier etc/msg (ne pas se fier au / initial, il correspond à la racine de l’initramfs), que vous pouvez créer pour insérer un message d’accueil sympathique, comme je l’ai fait :

    :::bash
    ^[[1;35m .
    .vir. d$b
    .d$$$$$$b. .cd$$b. .d$$b. d$$$$$$$$$$$b .d$$b. .d$$b.
    $$$$( )$$$b d$$$()$$$. d$$$$$$$b Q$$$$$$$P$$$P.$$$$$$$b. .$$$$$$$b.
    Q$$$$$$$$$$B$$$$$$$$P" d$$$PQ$$$$b. $$$$. .$$$P' `$$$ .$$$P' `$$$
    "$$$$$$$P Q$$$$$$$b d$$$P Q$$$$b $$$$b $$$$b..d$$$ $$$$b..d$$$
    d$$$$$$P" "$$$$$$$$ Q$$$ Q$$$$ $$$$$ `Q$$$$$$$P `Q$$$$$$$P
    $$$$$$$P `""""" "" "" Q$$$P "Q$$$P" "Q$$$P"
    `Q$$P" """
    ^[[0m^[[0m

Ça, c’est mon côté amateur d’art. Une fois fait, il faut donc compresser notre archive initramfs. Lorsque vous êtes à la racine de votre image, tapez *find | cpio -H newc -o | gzip -9 > /boot/initramfs* . Pour info, pour décompresser l’archive (si vous voulez la modifier, ou corriger un détail), placez-vous dans un dossier temporaire dans lequel vous modifierez l’image, et tapez *gunzip < /boot/initramfs | cpio -i* . Ensuite, avant de pouvoir rebooter, suivez le handbook sur l’installation et la configuration de Grub si ce n’est déjà fait, puis éditez le fichier */boot/grub/grub.conf* (sous grub-legacy, j’ai grub2 en horreur). Sous la ligne kernel que vous avez spécifiée, écrivez

    :::bash
    initrd /initramfs

Sauvegardez, respirez un bon coup, sortez du chroot (exit), démontez les partitions et points de montage de notre environnement, respirez à nouveau un bon coup, puis rebootez.

Je dois maintenant vous faire une confession : la majorité de ce guide a été écrite il y a plusieurs mois, et je n’ai pas pris le temps de le finir, ni de noter les péripéties qui me sont arrivées après le premier boot. Mais de mémoire, le kernel a bien booté, je me suis pris la tête pendant un moment avec l’initramfs, et puis ça a fini par marcher. J’espère qu’avec mes infos, vous parviendrez à un premier boot correct, à partir duquel vous pourrez commencer à personnaliser réellement votre système : pour un environnement de bureau, rien de tel que le [guide Xorg](http://www.gentoo.org/doc/fr/xorg-config.xml), suivi de l’installation d’un environnement comme KDE, Gnome ou un plus léger [Awesome](http://awesome.naquadah.org/).

Si toutefois le premier boot ne se passe pas bien, formatez intégralement vos disques et reprenez du début. Non je déconne. Rebootez sur le live-cd, et répétez les étapes permettant d’entrer dans le chroot (sans repartitionner, hein :) ). Une fois dans le chroot, et selon le problème, reconfigurez puis recompilez le kernel, éditez l’init script, ou modifier le fichier de configuration qui pose problème, puis répétez l’opération jusqu’à arriver à un boot complet.

Enfin, je tiens à rendre hommage à la communauté française (et québécoise) de Gentoo, particulièrement réactive, intelligente (rien à voir avec les discussions autour d’Ubuntu), qui prend généralement le temps d’aider et de conseiller les novices comme les plus aguerris (même si les « RTFM » fusent). Si vous êtes suffisamment courageux pour avoir suivi ce guide, vous êtes donc bienvenu(e) sur le chan IRC #gentoofr sur irc.freenode.net .

**Mise à jour : **merci à Skhaen pour la relecture et les corrections

**Repêchage : **J’ai repris cet article de mon ancien blog, parce qu’il m’a été souvent utile en tant qu’aide-mémoire, il n’y a pas de raisons pour que vous n’en profitiez pas.
