Title: Setup Yubikey et Bépo sous Gentoo
Category: Sysadmin
Permalink: setup-yubikey-et-bepo
Date: 2014-12-25
Modified: 2014-12-25
Author: Gordon
Tags: Yubikey, Yubico, Bépo, Gentoo
Status: draft

Je possède depuis un certain temps une [Yubikey], que je n’ai jamais pris le
temps d’utiliser dans mon workflow d’authentification. Victime d’un trop-plein
de temps libre, j’ai installé un environnement de test pour mettre en place une
meilleure authentification, d’abord sur ma machine personnelle, ensuite sur mes
serveurs. Compte-rendu détaillé.

### Quoi est Yubikey ?

Il s’agit d’une petite clé USB, reconnue comme un bête clavier (donc compatible
sur à peu près toute machine sachant manger un clavier USB), et permettant
d’effectuer de l’authentification par la possession. Pour éclaircir rapidement
ce point, lorsque l’on parle d’authentification, il s’agit de prouver qui on
est. Cela vaut pour un système informatique, comme pour une simple porte
d’immeuble. Et dans les deux cas, il existe plusieurs catégories de moyens pour
y parvenir :

### Authentification par la connaissance

C’est la plus courante sur un système informatique. Vous connaissez un secret
(un mot^W^Wune phrase de passe, typiquement), et être capable de le
répéter valide l’authentification. Hors informatique, on retrouve ça dans les
digicodes.

<div class="center"><img src="/images/yubikey/auth_connu.png" alt="L’authentification par la connaissance" /></div>

Faiblesse : un mot de passe, dans beaucoup trop de cas, ça se devine. Surtout
qand on donne la possibilité à des utilisateurs non sensibilisés à la sécurité
de les définir eux-mêmes. Les autres ne sont pas en reste, parce qu’il suffit de
sniffer le réseau au bon moment pour voler un mot de passe, et le compromettre
définitivement.

### Authentification par la possession

« Si tu possèdes ce truc unique, c’est que c’est bien toi ». Typiquement valable
pour les clés (physiques ou cryptographiques) : on vérifie si vous possédez le
bon machin, qui, lui, sait se rendre difficilement falsifiable. On ne peut donc
pas le copier facilement, et le but est que l’on doive présenter l’original pour
pouvoir passer.

<div class="center"><img src="/images/yubikey/auth_possession.png" alt="L’authentification par la possession" /></div>

Faiblesse : lorsque l’on vole une clé, on peut passer. Bien que la sécurité soit
renforcée par la conception même du *token* (le jeton d’authentification),
cette méthode d’authentification ne protège pas l’utilisateur de lui-même.

### Autres méthodes

On peut parler très rapidement de l’authentification par *ce que l’on est*,
caractérisé par la biométrie. L’application la plus courante est la vérification
des empreintes numér^Wdigitales. Vous savez, ces empreintes que vous laissez
littéralement **partout** ? J’ai besoin de vous dissuader de les utiliser pour
de l’authentification ?

<div class="center"><img src="/images/yubikey/auth_biometrie.png" alt="L’authentification par la biométrie" /></div>

Il en existe d’autres : authentification par l’origine (adresse IP, par
exemple), mais elles sont nettement moins intéressantes dans un contexte
générique.

### ALL O’THEM

Une authentification, d’un type ou de l’autre, c’est bien, mais pourquoi ne pas
les cumuler ? C’est ce qu’on appelle l’authentification forte : il faut pour
cela utiliser au moins deux méthodes distinctes : par exemple, une clé
cryptographique protégée par mot de passe. Encore que, cet exemple ne relève pas
exactement de la double authentification : on n’a pas tout à fait besoin de la
clé **et** de son code, mais de la clé déchiffrée (donc non, mon <a
href="/tag/gentoo.html">installation chiffrée de Gentoo</a> n’est pas si solide
que ça). Un meilleur exemple pourrait donc être un système acceptant un mot de
passe **et** un token unique pour valider une authentification. Ça tombe bien,
c’est le but ici avec la Yubikey.

## Quoi est Yubikey, disais-je ?

Maintenant que l’on a fait le point sur les méthodes d’authentification, on
comprend que la Yubikey est une méthode d’authentification par la possession :
si j’ai ma clé, je peux passer, peu importe ma mémoire.

Alors, techniquement comment ça marche ? Ça contient une clé cryptographique,
que l’on peut facilement remplacer (mais pas lire, car elle est en écriture
seule). Elle génère des signatures, permettant de prouver l’unicité de la clé.
Les signatures sont elles aussi uniques et incrémentales, de façon à ce qu’il ne
soit pas possible de s’authentifier deux fois avec la même clé, ou bien
d’« emprunter » une Yubi, de générer une poignée de codes, et de les utiliser
ensuite, car dès qu’un code est utilisé, tous ceux ayant été générés avant sont
invalidés.

La Yubikey possède deux slots, pouvant être configurés indépendamment par le
biais d’une interface pratique (`yubikey-personalization-tool` dans les paquets
Gentoo), et dans divers modes : pour faire simple, on peut générer une signature
par un appui sur la pastille de la clé, ou bien la configurer en mode
*challenge-response*, ce qui permet au système de demander directement une
génération. Un autre mode permet de faire du challenge-response avec
confirmation physique par un appui sur la pastille. C’est celui que
j’utiliserai.

Dernière chose, liée à mon environnement : pour entrer un code (j’utiliserai le
terme « OTP », pour « One-Time Password »), la Yubi simule une frappe clavier
correspondant audit code, puis un retour chariot. Le problème est que la frappe
clavier assume que l’on utilise une disposition *qwerty*, ce qui n’est
clairement pas mon cas. Il va falloir trouver un moyen de contourner ce
problème.
