Title: Vers le web Troipoinzéro !
Permalink: vers-le-web-troispointzero
Date: 2012-12-17
Modified: 2012-12-17
Tags: fsf, blog, python, pelican, web 2.0, grille-pain
Category: Blog

Ceux qui me lisent depuis longtemps peuvent percevoir une évolution constante dans ce blog. Ou une régression, si vous préférez. Le fait est que depuis la version 2 (sur gordontesos.com), je suis passé à une version épurée de tout script, voire de tout appel externe : aucune image n’était appelée sur un autre serveur, pas même les avatars fournis par Gravatar, qui étaient récupérées en local. Pas de boutons de partage social non plus, notamment parce qu’il s’agit de petites saloperies traceuses, mais également parce que les gens qui veulent partager un billet n’ont pas besoin d’un bouton clignotant pour le faire.

Et bien je passe un nouveau cap. Bienvenue sur la quatrième mouture du Kikooblog du Poney ! À première vue, peu de différences, mais si vous regardez bien :

* Les pages sont très sensiblement plus rapides à charger
* Le title du blog a évolué en conséquence (et il est 20% plus cool)
* Il n’y a plus de formulaire de recherche
* Il n’y a plus la liste de mes dernières publications sur [StatusNet](https://status.ndn.cx)
* Il n’y a plus de commentaires, ni de moyen d’en laisser

<!-- cut -->

### Wait, WHAT ?

Hormis le second point, tout s’explique par un fait très simple : le blog est maintenant statique. Donc, à l’heure d’une évolution parfois douteuse du web, je prends le sens inverse. Il y a deux vraies raisons derrière tout ça :

* Je prévois de ne plus faire mentir le footer du blog à court terme, et donc de véritablement héberger ce blog sur mon grille-pain. C’est un projet fun que j’ai réalisé, et dont je parlerai prochainement, une fois mis en prod. En tout cas, c’est une bête pas très puissante, et donc pour limiter sa charge, un blog en HTML pur est ce qu’il y a de mieux (les contenus lourds resteront hébergés sur un gros serveur, faut pas déconner).
* Wordpress devient trop lourd. En réalité, il l’a toujours été, mais je suis de plus en plus frustré qu’un moteur de blog se permette d’altérer mes textes, quand bien même je tape directement en HTML (par exemple, il convertit les « -- » en « — », ce qui est très gênant quand on colle du code). Il m’est inconcevable de ne pas avoir totalement la main sur mes contenus, même seulement par défaut, alors Wordpress ⇒ poubelle. Niveau sécurité, c’est également une avancée notable.

J’ai donc, influencé par [Rogdham](http://rogdham.net), choisi de tout migrer sous [Pelican](http://getpelican.com). Il s’agit d’un petit logiciel écrit en python, qui transforme tout bêtement du contenu écrit en Markdown (une syntaxe légère) en arborescence HTML, en y collant un template utilisant la syntaxe [Jinja](http://jinja.pocoo.org) (qui tend à se standardiser). J’ai donc réécrit entièrement le template de la v3 du blog, en gardant simplement le CSS. Si vous vous posez la question, sachez que ça a été très rapide et simple. Pelican tourne donc sur ma machine en local, j’écris mes billets, et grâce à un joli Makefile, je génère l’intégralité des pages. Avec une commande supplémentaire, j’uploade ces pages sur le serveur. En fait, c’est un fonctionnement tout bête mais efficace. Avec un peu de git pour versionner tout ça et y accéder facilement sur n’importe lequel de mes postes, ça donne une possibilité de rédaction assez souple. Du coup, le serveur se retrouve à ne servir que du HTML pur, ce qui a de nombreux avantages. Mais aussi des inconvénients :

* Plus de recherche possible, vu qu’il n’y a plus de code applicatif sur le serveur. Mais vous pouvez naviguer via les catégories et tags.
* Plus de liste des derniers messages de micro-blogging, mais il suffit de les consulter directement via l’interface web, un client dédié ou autre.
* Plus de commentaires. C’est le véritable point sensible. Notamment parce qu’il est arrivé d’avoir de très intéressants débats en commentaires. Pour l’heure, vous pouvez toujours réagir par mail(<blog@gordon.re>), j’éditerai les billets pour inclure les contributions. Peut-être que ça incitera à écrire de vrais billets argumentés en réponse, et ça serait une très bonne chose. Et pour les réactions courtes, il semble que le micro-blogging fasse l’affaire.

C’est tout pour aujourd’hui. Il reste quelques détails à peaufiner sur ce nouveau blog, mais tout devrait être corrigé rapidement.
