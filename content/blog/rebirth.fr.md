Title: Rebirth
Permalink: rebirth
Date: 2016-02-15
Modified: 2016-02-15
Tags: mise à jour
Category: Blog

Nouvelle version du blog !

Le dernier article remontant au 14 juillet… 2014, on était en droit de penser
que le blog était tombé à l’abandon. Il n’en est rien, je n’avais simplement
pas pris le temps de reprendre la plume.

C’est maintenant chose faite, et je profite de cet élan de motivation pour
rafraîchir le contenant, occasionnant ainsi une nouvelle version, comme dit
plus haut.

<!-- cut -->

### La recherche !

Lorsque [j’annonçais]({filename}vers-le-web-troipointzero.fr.md) ma
migration à [Pelican](http://getpelican.com), c’était l’un des points dont
j’acceptais la disparition. À la longue, j’ai regretté de ne pas pouvoir
accéder facilement à un article (ce dont j’avais besoin assez régulièrement).
Je reviens donc sur mes précédents propos : il est possible, avec Pelican,
d’avoir un moteur de recherche interne. Ça se fait grâce à un bout de code
Javascript, [Tipue](http://www.tipue.com/search/), qui va récupérer un fichier
d’indexation (en JSON), et effectuer une recherche là-dedans, avant de dérouler
les résultats de la recherche sur une page statique.

Je reviens donc sur ma précédente volonté de ne pas inclure de JS sur mes
pages. Néanmoins, le blog est parfaitement consultable sans JS, comme avant.
Les deux utilisation de JS sont des intégrations de logiciels libres, et
apportent uniquement de nouvelles fonctionnalités.

Le fonctionnement est donc le suivant : j’utilise le plugin
[tipue-search](https://github.com/getpelican/pelican-plugins/tree/master/tipue_search)
qui génère, lors de la génération du site, un fichier JSON placé à la racine.
Ensuite, il n’y a qu’à intégrer le formulaire de recherche dans le template, et
tout fonctionne à peu près automagiquement. Je me suis basé sur l’intégration
du template [elegant](https://github.com/talha131/pelican-elegant) pour faire
la mienne. À noter, j’ai récupéré la dernière version de Tipue plutôt que de
récupérer les fichiers fournis avec le plugin Pelican.

### Les commentaires

Plus loin dans la contradiction avec [mon ancien
moi]({filename}../hacktivisme/la-necessite-des-commentaires.fr.md), j’ai décidé
d’intégrer des commentaires, de nouveau (de mémoire, la v3 de ce blog tournait
sous Wordpress). Là encore, générateur statique oblige, il n’est évidemment pas
possible de sauvegarder les commentaires côté serveur par Pelican, puisque
Pelican reste sur ma machine locale, et se contente de déployer des fichiers
statiques en ligne. Il est cependant possible d’intégrer dans Pelican un
système de commentaires, tel que Disqus (vous me voudrez pas de ne pas faire de
lien vers une plateforme centralisée et privatrice), mais évidemment, cette
solution est loin de me convenir. J’ai cependant entendu parler d’alternatives
libres et installables, parmi lesquelles [Isso](https://posativ.org/isso), qui
a retenu mon attention par la simplicité de son approche. En gros, il s’agit
d’installer Isso, qui est une petite webapp, sur un sous-domaine du blog, et
qui va servir une API consommable en JS. En intégrant un script dans la page,
on discute avec cette API, et on affiche les commentaires, et le moyen d’y
répondre. C’est très léger, c’est très simpliste, on peut commenter anonymement
et répondre aux commentaires, et c’est en Python (*et c’est le nom allemand
d’un Pokémon ♥*), donc j’ai fait mon choix.

Pour la mise en place d’Isso, je vous laisse consulter cet excellent billet en
français sur l’[installation d’Isso et son intégration dans
Pelican](http://hackriculture.fr/isso-commentaires-auto-heberges-pour-pelican-et-autres-sites-statiques.html).
Je n’ai rien d’intéressant à y ajouter, si ce n’est que vous devriez jeter un
œil régulièrement à ce blog.

Au-delà du point de vue technique, vous pouvez donc de nouveau commenter mes
articles. Amusez-vous bien.

### L’internationalisation

J’ai toujours écrit en français sur mon blog, mais certains billets techniques
pourraient être utiles à des non-francophones. J’ai donc décidé de rendre ce
blog bilingue (français/anglais). Je rédigerai toujours en priorité mes
articles en français, puis, dans la mesure du possible, je les traduirai en
anglais. Il est possible que je ne le fasse que pour les articles techniques,
ça dépendra du temps que ça me prend.

Techniquement (ce qui implique que cet article sera traduit), j’ai donc :

- utilisé la fonctionnalité intégrée à Pelican permettant de rédiger un même
  contenu (article, page) dans plusieurs langues
- installé le plugin
  [i18n-subsites](https://github.com/getpelican/pelican-plugins/tree/master/i18n_subsites),
  permettant de générer des sous-sites localisés. En consultant
  <https://gordon.re/en/>, vous serez donc sur la version anglophone du site. Les
  articles non traduits seront tout de même affichés dans leur langue
  originale
- intégré l’<abbr title="internationalisation">i18n</abbr> dans mon template.
  Ce qui permet d’avoir une lecture beaucoup plus soignée sur la version
  anglophone

Ce qui n’est pas encore fait :

- les catégories ne sont pas, pour le moment, internationalisées. Je n’ai pas
  encore trouvé comment faire (et pour être franc, je ne m’en suis pas vraiment
  occupé)
- l’index de recherche, généré par Tipue, ne tient pas compte de la langue. Il
  ressort donc toutes les correspondances, dans toutes les langues. Ce qui peut
  être gênant lorsqu’on recherche des termes anglophones (ce qui est plus que
  susceptible d’arriver sur des articles techniques…). Ça peut être
  problématique, et je tâcherai de corriger ça.

### Refonte CSS

J’ai beau être satisfait de l’apparence du blog, je dois reconnaître que sa
réalisation n’est pas très propre. J’ai préféré sous-traiter la tâche de
refaire l’intégration à [quelqu’un de bien plus compétent que moi dans le
domaine](https://robinberthaud.fr). C’est encore en cours, mais il ne devrait
pas y avoir de changement bien notable (à part des bugs en moins).

Ce n’est pas du CSS, mais j’en profite pour notifier également le changement de
nom du blog, ainsi que son sous-titre : sensiblement plus sobre, à l’image des
futurs articles.

### Changement de cap sur le HTTPS

Jusqu’à présent, je gérais ma propre
[CA](https://fr.wikipedia.org/wiki/Autorit%C3%A9_de_certification) pour servir
ce blog, et mes autres sites, en HTTPS. Évidemment, ça imposait un
avertissement de sécurité à quiconque ne lui faisait pas confiance, c’est
à dire tout le monde, et [à
raison]({filename}../hack/brainstorming-hacking-ssl.fr.md).

Avec la création de [Let’s Encrypt](https://letsencrypt.org/), je peux
maintenant générer de façon quasi-automatique des certificats gratuitement,
j’en profite pour abandonner ma CA, et passer mes domaines chez Let’s Encrypt.
Oui, ça reste une CA qui fait du X.509, et c’est un modèle que je considère
comme cassé. Ce n’est donc pas la meilleure solution, loin de là. Mais ne
pouvant pas me battre sur tous les fronts à la fois, je fais un compromis ici.

Le script automagique fourni par Let’s Encrypt ne satisfaisant pas mes besoins
(notamment parce qu’il doit tourner en *root*), j’ai préféré en utiliser un
autre, parlant le même protocole :
[letsencrypt-nosudo](https://github.com/gordon-/letsencrypt-nosudo)

### Changement d’hébergeur

Avec [FAImaison](https://faimaison.net) qui est enfin [devenu opérateur
IP](https://www.faimaison.net/actualites/installation-datacenter-host-reze.html),
j’ai choisi de louer un espace dans le quart de baie de l’association pour
y installer une machine. C’est donc probablement le premier site web hébergé
intégralement sur l’infrastructure FAImaison, et je contrôle maintenant la
quasi-totalité de mon service (il y a des faiblesses sur la redondance réseau
et électrique, certes).

C’est à peu près tout pour cette mise à jour. Je vais tâcher de garder une
bonne dynamique et de republier régulièrement. D’ailleurs, j’ai une poignée de
choses à dire.
