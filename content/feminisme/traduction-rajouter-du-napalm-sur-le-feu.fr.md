Title: [Traduction] Rajouter du napalm sur le feu
Permalink: traduction-rajouter-du-napalm-sur-le-feu
Date: 2013-03-01
Modified: 2013-03-01
Tags: okhin, sexisme, parti pirate
Category: Féminisme

Ça fait longtemps que je n’avais pas traduit de [billet
d’Okhin](https://about.okhin.fr/posts/Bring_Moar_Fire/). Celui-ci est
particulièrement intéressant, et parle à nouveau de sexisme.

Si vous avez la flemme de tout lire, voici un résumé : 

> Allez vous faire voir, arrêtez d’être fainéant·e et lisez.

<!-- cut -->

### Rajouter du napalm sur le feu.

#### Mise au point

Je suis privilégié. Peu importe ce que je peux dire sur l’état de ce monde, il
est clair que je suis né de son meilleur côté. Je peux m’exprimer sans risquer
d’être frappé ou torturé. Je peux traverser la rue pour acheter à manger sans
risquer de me faire abattre par un sniper. Je sais que je dors au chaud chaque
nuit, et que je peux avoir trois repas (ou plus) par jour (tant que je n’oublie
pas de les prendre).

Et je ne serai pas insulté, agressé, violé, considéré comme une minorité, je ne
me sentirai pas en danger pour le simple fait de marcher dans la rue.

Tout ça parce que je suis un homme blanc. J’ai acquis des privilèges (que je
n’ai pas demandés) par le simple fait d’être né. Et c’est naze. Je veux dire, le
fait d’avoir des privilèges implique que j’aie du pouvoir sur d’autres
personnes. Et c’est naze, parce que ça signifie que ces personnes (celles sur
lesquelles j’ai du pouvoir) ne sont pas libres, ce qui entrave ma liberté (si
les personnes autour de moi ne sont pas libres, je ne peux pas bénéficier de ma
liberté).

Alors oui, être privilégié rend la vie plus facile, mais ça ne va pas. Je ne
veux pas de ça. Et se débarasser de ça prendra beaucoup de temps, parce que la
société dans laquelle je vis a besoin de changer beaucoup plus globalement. Et
ça commence en sensibilisant sur la situation (et ensuite en la changeant en
abandonnant ce pouvoir).

#### Faits et statistiques

S’il n’y avait aucune discrimination dans l’éducation, alors les compétences
seraient également réparties dans la population, et donc vous devriez trouver
des personnes compétentes n’importe où. Je veux dire, si 20% de la population
avait la peau bleue, alors il devrait y avoir 20% de personnes à la peau bleue
parmi les gens qui cuisinent. Ça vous semble correct ?

Donc, si notre système éducatif fonctionne bien, il devrait tendre à développer
l’intérêt et la curiosité de façon égale. Alors, le fait que j’aie rencontré 5
femmes depuis que j’ai commencé mes études dans un domaine technologique (une
dans une société, les 4 autres étaient des camarades de classe) est soit une
erreur statistique, soit la preuve que le système ne fonctionne pas aussi bien.
J’ai rencontré d’autres femmes dans les départements technologiques dans
lesquels j’ai travaillé, mais elles occupaient principalement des postes
créatifs (design, intégration…).

Il y a donc quelque chose de cassé là-dedans. J’ai un problème pour rester
longtemps dans une société : depuis 13 ans que je travaille (oui, j’ai commencé
tôt), hormis la société dans laquelle j’ai fait mon apprentissage, je n’ai
jamais passé plus d’un an dans la même entreprise. Donc ça en fait 8. De
différentes tailles, de différents horizons.

Et bien, je n’ai jamais rencontré une femme dans les départements IT de ces
boîtes. Parfois, c’était moi le département IT, mais même dans les équipes de
développement, je n’ai jamais rencontré une seule femme. Les seules femmes que
j’ai rencontrées viennent de la scène hacker (et la plupart du temps, je ne
l’apprenais qu’en les rencontrant dans le *meatspace*, mais c’est un autre
sujet).

Donc, quand quelqu’un me dit, au sujet du sexisme, que « si ce n’est pas cassé,
il ne faut pas essayer de le réparer » comme un argument pour ne pas penser à
des politiques anti-harcèlement, je pense qu’ils ont tort. C’est un problème.

#### Un politicien sauvage apparaît !

L’autre jour (deux ou trois jours avant cette rédaction),
[@_LaMarquise](https://twitter.com/_LaMarquise/) a été agressée dans la rue par
un type se masturbant en public, et elle en a parlé sur twitter. Un illuminé de
service, [@romain_pp](https://twitter.com/romain_pp), a trouvé pertinent de faire
une plaisanterie à ce propos. Le fait est que cette personne est membre du Parti
Pirate Français et Suisse, et si j’ai bien tout compris au fonctionnement de ce
parti, n’importe qui peut parler en son nom. C’est même écrit dans le nom de son
compte twitter, dans sa description, et même dans son image d’arrière-plan.
Alors ouais, c’était la parole du Parti Pirate.

Les choses se sont envenimées sur Twitter, et la principale argumentation contre
La Marquise était qu’elle n’était pas rationnelle. Je vais développer plus tard,
mais basiquement, je tend à penser qu’on ne peut pas attendre de quelqu’un sous
le choc d’être rationnel.

On lui a aussi dit qu’elle était agressive, qu’elle ne devrait pas rendre
publiques ces choses personnelles, comme une agression (très bien, alors
pourquoi est-ce que les gens tweetent à propos de leur vie privée ?), qu’elle ne
devait pas déranger leur petite tranquilité.

Le Parti Pirate a écrit une lettre en réponse à La Marquise. Ils l’ont fait en
privé (étant donné que je n’ai pas pu la trouver en ligne). Ce que je trouve
étrange pour un parti qui défend la transparence à tous les niveaux de la
société. Ceci dit, les systèmes informatisés sont sympas, parce qu’ils
permettent de copier les choses à coût négligeable, et donc voici [une copie de
cette lettre](http://www.twitlonger.com/show/l69r7b) (elle a été fournie par
Marquise, et je n’ai aucune raison de douter de sa parole là-dessus). En
substance, ils disent regretter ce que l’un de leurs membres a dit, et également
le « buzz » qui s’en est suivi. Ils n’ont pas saisi l’opportunité d’opter pour
une position plus active, pas plus qu’ils n’ont blâmé le membre.

Basiquement, la lettre est une tentative d’effort pour calmer le jeu sans
prendre réellement position pour ou contre le sexisme. S’ils sont contre le
sexisme, ils devraient, au moins, renvoyer Romain, sinon ils n’auraient pas
besoin d’écrire à ce sujet. Cette lettre prouve que ce qui est important pour
eux est d’éteindre l’incendie médiatique plutôt que défendre une position.

Ce qui est honteux est également qu’ils sont généralement les premiers à blâmer
ce genre de comportement dans les autres partis. Il y a aussi un problème
concernant la liberté d’expression, mais j’y reviendrai.

#### Au sujet de la violence

Vivre dans la peur d’être agressé·e ou violé·e n’aide pas à garder la tête
froide. Comme je l’ai dit (et d’autres l’ont dit aussi), garder la tête froide
est un privilège des gens qui ont du pouvoir, ne l’oubliez pas. L’insurrection,
et la nécessité du changement, mènent à la violence, c’est inévitable. [Ce
billet](https://violenceparfoisoui.wordpress.com/2013/01/24/timult-critique-ideologie-non-violence/)
l’explique assez bien, et cette citation est intéressante :

> La soumission des opprimés est liée à l’ordre établi. C’est le fait de
> déranger cet ordre en brisant ses chaînes, et en s’en prenant aux maîtres qui
> est perçu comme un scandale. Dans le langage des maîtres, qui devient le
> langage courant, la violence ne vient pas de ceux qui commettent cette
> violence, mais des vilains qui osent se rebeller.
>
> -- 
>
> **Igor Reitzman**

Quand quelqu’un vous crie dessus à propos de quelque chose, vous devriez
l’écouter, parce que cette chose est importante pour la personne (si ça ne l’est
pas pour vous). Vous n’imagineriez pas les Révolutionnaires Français demander
gentiment à Louis XVI s’il accepterait de bien vouloir céder le pouvoir. Un tas
de personnes ne voudrait pas abandonner le pouvoir, et il faut parfois les
forcer à le faire.

Ça m’a pris un moment pour comprendre ça, parce que ce n’est jamais agréable
quand des gens vous hurlent dessus. C’est irritant, et on a tendance à répondre
à l’agressivité par l’agressivité. Je ne suis pas sûr d’être totalement
d’accord avec ça, mais j’essaie de comprendre les raisons pour lesquelles les
gens crient (et j’essaie aussi de ne pas répondre trop rapidement, parce qu’en
général ça n’aide pas la situation, quelle qu’elle soit).

Alors oui, des féministes utilisent la violence, qu’elle soit physique ou
morale. Et si ça vous dérange, ça veut dire que ça marche. Vous devriez poser
des questions et tenter de comprendre pourquoi elles sont en colère, au lieu de
leur dire de se calmer.

#### Au sujet de la liberté d’expression

Par ailleurs, je suis contre la censure. Ça veut dire que je condamne le fait de
brider la parole. Je veux que les nazis puissent exprimer leurs idées, car c’est
comme cela que vous pourrez trouver leurs idées dangereuses. Et je veux que les
misogynes puissent le faire aussi, parce que c’est comme ça que vous les
reconnaîtrez. Et c’est également le seul moyen d’échanger avec eux autour du
problème.

Mais la liberté d’expression va dans les deux sens. Ce n’est pas parce que
quelqu’un est autorisé à dire quelque chose qu’il n’a pas le droit d’être
contredit, décrié, puni ou autre. Vous avez le droit de faire des blagues
sexistes. Et j’ai le droit de vous dire qu’elles ne sont pas drôles. J’ai même
le droit de le dire à tout le monde. Si vous ne le voulez pas, et que vous
préférez pouvoir dire ce que vous voulez sans conséquences, alors vous défendez
la censure.

Alors oui, ça me dérange, ce qui est arrivé à BSides (voici [le point de vue de
Violet](http://violetblue.tumblr.com/post/44107008572/what-happened-with-my-security-bsides-talk)
, et [celui de
AdaInitiative](https://adainitiative.org/2013/02/keeping-it-on-topic-the-problem-with-discussing-sex-at-technical-conferences/).
Pour résumer, une conférence sur le sexe et les drogues, qui avait été annoncée,
a été supprimée de l’agenda à cause de la peur d’une chasse aux sorcières par
l’équipe du BSides (que ce soit à l’initiative d’AdaInitiative ou non n’est pas
très clair à mes yeux) au prétexte qu’il pourrait y avoir des victimes de viol
qui auraient pu être stressées (il semble que c’est comme ça que les troubles de
stress post-traumatiques) si elles avaient assisté à la conférence, et que
parler de la façon dont les drogues fonctionnent, en particulier le GHB, dans
une conférence nommée « Sexe et drogues : vulnérabilités connues et méthodes
d’exploitation » serait une incitation au viol.

L’argument est que, dans les conférences de hack, les gens font des sujets
nommés « Vulnérabilités connues et méthodes d’exploitation » pour encourager
l’exploitation de ces vulnérabilités. Et bien c’est une idée fausse. La plupart
de ces conférences se préoccupent plus d’expliquer comment se protéger contre ces
vulnérabilités plutôt que comment les exploiter.

En général, ces vulnérabilités sont patchées au cours de la conférence, ou du
moins, les gens qui gèrent le logiciel en question travaillent dessus (s’ils
prennent leur boulot au sérieux, je veux dire). Bien sûr, certains les utilisent
pour leur propre profit, mais il s’agit d’une minorité.

Et en réalité, les gens qui utilisent ces vulnérabilités pour leur propre profit
n’ont pas intérêt à ce que celles-ci soient connues. Les rendre publiques est de
la prévention et de l’éducation, ce n’est pas fait pour mettre en danger les
gens. C’est comme ça que la prévention fonctionne.

Maintenant, faut-il faire de la prévention dans la communauté technophile ? Bien
sûr qu’il le faut. Il y a tout un historique d’agressions sexuelles et de viols
dans ce genre de conférences. Si vous refusez d’en parler, vous ne pouvez
éduquer les gens et vous ne les changerez pas. AdaInitiative disent qu’ils
organisent leur propre *camp* pour en discuter. Mais c’est comme faire une
campagne de prévention des drogues dans un camp de scouts : vous n’aiderez pas
des drogués à gérer leur addiction.

Alors oui, il nous faut éduquer nos camarades hackers, particulièrement lors
d’occasions où il y a beaucoup d’alcool, de drogues et de manque de sommeil,
parce que tout cela change votre perception des choses. Alors en parler est une
nécessité. Et, si la discussion devient offensante, alors les gens doivent le
dire et le condamner, mais vous ne pouvez pas savoir comment ça se passera avant
que ça n’arrive.

Il y a toujours ce problème avec les victimes de viols et les <acronym
title="Troubles de Stress Post-Traumatiques">TSPT</acronym>. Je peux comprendre
que des gens ayant subi une agression et/ou un viol n’aient pas envie d’être
exposés à une conférence qui en parle (hé, c’est à eux de gérer leur souffrance
comme ils l’entendent). Et il semble qu’il y ait une coutume autour des
« trigger warnings », que je ne comprends pas tout à fait (ça semble fonctionner
un peu comme les labels « PEGI » pour les jeux vidéos).

#### Fin

Hmm, je pense avoir loupé plusieurs points. Ou je peux me planter sur d’autres.
Si vous le pensez, faites-le moi savoir. Vous pouvez me trouver assez
facilement.
