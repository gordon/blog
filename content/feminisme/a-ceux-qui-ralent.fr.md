Title: À ceux qui râlent
Permalink: a-ceux-qui-ralent
Category: Féminisme
Tags: sexisme, geeks, gamers
Date: 2013-04-22
Modified: 2013-04-22

Je n’ai pas l’habitude d’écrire ce genre d’articles. Il s’agit d’une réponse à
un billet de [quelqu’un que je considère encore comme un ami de
valeur](http://pixellibre.net), [au sujet du féminisme, ou plus exactement des
débats qu’il occasionne ces derniers
temps](http://pixellibre.net/2013/04/0-1-comprenne-qui-pourra/). J’ai
initialement voulu répondre directement en commentaire, mais parce qu’il avait
fait le choix de les fermer, je me retrouve à écrire ici. J’en profite pour
mettre à plat certaines de mes opinions à ce sujet, en espérant ne pas trop
faire doublon avec mes articles précédents, notamment
[celui-ci](http://gordon.re/feminisme/je-ne-suis-pas-feministe-je-suis-egalitariste.html).

<!-- cut -->

### Le féminisme extrémiste (celui avec des grandes dents)

Bon tout d’abord, je ne sais pas combien de fois il va falloir le rappeler : il
n’y a pas « le » féminisme. Pour rappel, il s’agit de revendication de
l’égalité des sexes et des genres. L’interprétation là-dessus est libre, ce qui
donne un tas de militantismes différents. Et crois-moi Jérôme (prenez
l’habitude, je m’adresse à une personne particulière dans ce billet), le
féminisme extrémiste, ce n’est pas ce que tu vois. Le féministe extrémiste
flirte sans gêne avec la misandrie, et ça se voit très vite. Et de tou·te·s les
féministes que je connais, personne ne soutient ces idées. Si vraiment tu veux
lire ce que je considère comme du féministe extrémiste (ce qui est revendication
de l’auteure), je te conseille de lire
[ceci](https://jeputrefielepatriarcat.wordpress.com/2013/03/28/le-coit-une-aberration-totale-sauf-si-on-regarde-sa-fonction-dans-le-patriarcat/).
Je préfère te prévenir, c’est *vraiment* violent. Ce que tu appelles « féminisme
extrémiste », ce sont des gens très visibles sur twitter qui passent une grande
partie de leur temps à se plaindre. Se plaindre de situations vécues, de
réponses qu’on leur fait, d’articles lus, etc. Crois-moi, personne ne fait ça de
gaieté de cœur. Seulement, quand on est sensibilisé au problème du sexisme, on a
la fâcheuse tendance à le voir partout. Et tu sais particulièrement combien il
est difficile de laisser couler des injustices qui sont sous notre nez.

### La pilule rouge

Le fait est que ton article **dénonce**. Et qu’est-ce qu’il dénonce ? Le
comportement de certain·e·s féministes. Ce qui est intéressant (et ce qui
découle également de la discussion eue plus tôt avec deux autres hacktivistes),
c’est justement le sens de cette dénonciation. Pierre, Élodie ou toi, vous êtes
évidemment contre le sexisme. Dans les deux sens, je suis d’accord. Et quand
vous écrivez sur le sujet, c’est immanquablement pour… taper sur des féministes.
C’est révélateur, tu ne trouves pas ? N’aurait-il pas été plus utile, efficace
ou que sais-je de dénoncer le sexisme *concrètement* ? Par exemple, dans ton
billet, tu évoques des agressions que tu as subies, et que tu prends avec
légèreté. Chacun réagit à sa façon à ce genre d’actes, et je n’ai rien à redire
à la façon dont tu le prends. Contrairement à ce que l’on peut croire, je ne
considère en aucun cas qu’être victime d’agression doit marquer à vie et nous
forcer à vivre dans la honte et la culpabilité. Par contre, notre culture joue
un rôle important là-dedans, dans le fait que pour toi ça n’ait pas été très
grave. Car beaucoup de femmes prennent ce genre d’acte bien moins légèrement que
toi. Peut-être que leur fréquence y est pour quelque chose ? Je soupçonne que
oui, mais je n’en ai pas la preuve. Par contre, ce qui est avéré, c’est la
**culture du viol**. Le fait que les jeunes filles, dès la puberté, apprennent à
avoir peur, à redouter les agressions sexuelles. Agressions qui arrivent
d’ailleurs en très grande majorité dans le cercle privé ou familial, mais c’est
un autre sujet. Tu ne peux ignorer qu’on conseille aux femmes de ne pas sortir
seules le soir, de faire attention à elles, etc. Face à ça, comprends-tu la
gêne, la peur, quand un quelconque gentleman aborde une femme, qui n’a rien
demandé, dans un espace qu’elle considère culturellement comme *dangereux* ?
Sachant également que ce qu’on leur apprend à craindre est considéré comme
banal, ne serait-ce qu’au moment de porter plainte si l’on en a le courage
(difficile de porter plainte contre un membre de sa famille, surtout lorsque
c’est sur nous que sera jetée la honte dans la majorité des cas…). Vois-tu où je
veux en venir, et pourquoi **ton** ressenti face à des agressions (qui sont
condamnables avec la même fermeté que si tu avais été une femme, fût-il utile de
le préciser) est forcément différent de celui d’une femme ?

On me reproche, face à ça, de considérer les femmes comme des victimes en
puissance. Ce n’est pas vrai. Les femmes sont **ciblées** par des actes sexistes
à différentes échelles. Elles les subissent. Mais pourquoi une agression
devrait-elle entraîner la honte ou peur de sa cible ? Encore une fois, toute
cette discussion part de personnes qui ouvrent trop leur gueule. Qui n’ont
justement pas du tout une posture de victime, mais au contraire, décident de ne
plus se laisser faire, qu’il s’agisse de se défendre d’une agression physique ou
de protester contre un geste commun de la vie quotidienne, mais qui participe à
la société patriarcale. Car, quand on choisit la pilule rouge, qu’on accepte de
remettre en cause notre culture, notre société, et, au fond, nous, on voit
l’envers du décor, la matrice. On voit à quel point le sexisme est *partout*, et
on perd facilement les pédales. C’est tout simplement ce qui arrive lorsque
quelqu’un comme moi se met à réagir au quart de tour sur twitter. On pense
constamment « Mais comment ne peuvent-ils pas voir ces injustices ? ». Je me
trompe peut-être à ce sujet, mais je vous vois (vous, qui écrivez pour dénoncer
l’attitude ou les propos de féministes énervé·e·s) comme des personnes, non pas
ayant choisi la pilule bleue, mais surtout qui n’ont pas été confrontées à ce
choix. Imaginez le film *Matrix* si, au lieu de latter des vilains en costard et
lunettes noires, les héros avaient pour but d’expliquer à la population qu’ils
sont dans la matrice… J’aime à penser que ça donnerait des situations similaires
à ce que nous vivons. Ceci dit, dans mon analogie, le camp des héros est
clairement défini, et il s’agit comme par hasard de celui que je m’attribue.
J’aimerais beaucoup connaître votre avis là-dessus.

Quoi qu’il en soit, une fois la pilule rouge avalée, on commence à remettre en
question notre environnement. Lors d’un débat, on s’aperçoit que la seule femme
présente n’arrive pas à en placer une ou à se faire écouter. On remarque
également les remarques, davantage tournées vers son apparence que vers ses
idées. On tend un peu plus l’oreille quand on entend une femme se plaindre du
comportement des gens dans la rue (et pas seulement des hommes). Parfois, et
c’est plus difficile, on se prend soi-même à lâcher une parole qui autrefois
n’aurait pas posé problème, mais qui aujourd’hui sonne faux, parce que vous
remarquez qu’elle est discriminante et gratuite. Ça peut être quelque chose
d’aussi insignifiant en apparence que le fait d’user de « Mademoiselle » pour
s’adresser à une jeune femme, ou bien un glacial « t’as vu comment tu t’es
habillée, aussi ? » à une femme qui raconte comment elle a failli être violée
quelques instants plus tôt. Je comprends que cela provoque un agacement : on
déterre toutes ces petites choses qui composent notre vie de tous les jours,
toutes ces choses acquises, avec lesquelles on vit. Et remettre en question sa
culture est une chose quand on a choisi d’ouvrir les yeux, mais se le prendre en
pleine gueule alors qu’on ne s’en soucie pas peut effectivement mettre très mal
à l’aise, et occasionner le l’agressivité en signe de défense.

C’est ce qui arrive lorsque que l’on dénonce le sexisme dans la société, mais
c’est exactement pareil dans les communautés, comme les geeks. À la différence
près que, quand on s’identifie comme geek, on s’attache à cette culture. Et
inévitablement, on tombe dans le communautarisme. Que ce soit pour le milieu
geek ou les autres. La réponse hautaine qu’on va tenir à un nouveau qui demande
des conseils candides (et parfois un peu idiots) dans notre domaine d’expertise,
c’est du communautarisme. On a notre communauté, et on veut naturellement
s’assurer qu’on n’y entre pas n’importe comment, car c’est un espace familier.
Quand on tape parfois brutalement sur [Tris](https://twitter.com/tris_acatrinei)
quand elle parle de sécu, quand bien même c’est argumenté, c’est du
communautarisme, et je reconnais en faire moi-même. Voyez, je reconnais
reproduire le problème, et j’en suis conscient. Et pourtant, je continue à lui
dire ce que je pense, ce qui est rarement glorieux. Ridicule, vous ne trouvez
pas ? D’ailleurs, je ne passe pas à côté du sexisme très installé dans certaines
œuvres, comme la série *Game of Thrones*. Et pourtant, je l’aprécie. Parfois, le
fait d’avoir cette analyse inconsciente des constructions genrées dans la
fiction gâche le plaisir de la découverte. C’est comme ça, c’est ça le revers
de la pilule rouge. On voit le monde encore plus sombre qu’avant. Mais on sait
qu’on a alors les clés en main pour changer ça à notre échelle, en commençant
par moins reproduire ces schémas. « Moins », et pas « plus », car la tâche est
monumentale, et la seule façon de refuser du jour au lendemain toute forme de
sexisme serait de couper tout lien avec la société, et de subir un lavage de
cerveau. Ce qui ne serait pas très efficace. Alors on accepte de vivre dans ce
monde sexiste, on continue d’aprécier des œuvres, même si elles se montrent
sexistes. Il faut savoir faire des compromis, et connaître les limites de son
militantisme.

### Revenons au sujet

Dans ton article, Jérôme, tu persistes à expliquer en quoi tu réfutes le terme
de féminisme pour parler d’égalité. Je suis égalitariste, tu le sais. Je ne
souhaite pas inverser les privilèges, je souhaite que femmes et hommes soient au
même niveau social, aient les même chances par défaut dans la vie.

Mais.

Être pour l’égalité sans aller plus loin, c’est une bien belle déclaration, qui
n’a d’égale à son ardeur que son inutilité. C’est une déclaration de principe,
rien de plus. Parce qu’il est impossible d’évoquer l’égalité hommes-femmes sans
évoquer le patriarcat. Or, je te n’ai jamais vu ne serait-ce qu’employer ce mot.
Sais-tu seulement ce qu’il signifie ? Il signifie que la société dans laquelle
on vit a été pensée par et pour des hommes, et que les femmes y sont réduites à
l’état de ressource. Évidemment, la société a évolué, et la place des femmes
avec elle. Reste qu’historiquement, la domination était très claire : les hommes
gouvernaient, et les femmes obéissaient. Aujourd’hui, est-ce différent ? « On
n’est plus dans les années 50 », mais on n’est pas encore dans les années
10 000. Autrement dit, il y a **beaucoup** de chemin. Des exemples ? Le plafond
de verre, qui symbolise l’extrême difficulté qu’ont les femmes à atteindre les
postes à responsabilités. Ou l’inégalité salariale, ou encore la culture du
viol, conçue pour culpabiliser les victimes et souvent d’excuser les auteurs. Ou
encore la banalité que représente l’abordage d’une femme dans la rue, qui n’a
rien demandé. Le patriarcat enseigne qu’on le peut, et qu’une femme doit se
faire belle pour plaire aux hommes.

Le patriarcat, c’est une composante majeure de notre société. Il est normal que
ça couine un peu quand on tape dessus. En tout cas, si tu souhaites lutter pour
l’égalité, il y a une chose importante à faire. Et difficile. C’est le choix de
la pilule, dont je parlais plus haut. Admettre l’existence du partiarcat, pour
commencer (c’est pas si évident). Et surtout, admettre qu’en tant qu’homme, [tu
es naturellement
privilégié](http://www.crepegeorgette.com/2013/04/22/moi-valerie-je/). Ce n’est
pas une honte, une critique ou quoi que ce soit. D’ailleurs tu n’y peux rien,
tu n’as pas choisi ton genre. Mais c’est un fait, tu fais partie de la « caste »
mise en avant par le patriarcat. Il y a fort à parier que l’immense majorité des
gens qui liront ce billet sont privilégiés, d’une façon ou d’une autre. Toi,
Pierre et moi, nous sommes des hommes blancs hétérosexuels cis (c’est à dire qui
vivons notre genre assigné à la naissance). Cela fait 4 raisons d’être
privilégiés. Nous sommes réellement quasiment au sommet de la pyramide sociale
des discriminations. Ce sont les femmes qui sont victimes du patriarcat. Ce sont
tous ceux dont la couleur de peau n’est pas celle des anciens conquérants
européens qui sont victimes du racisme. Ce sont les homosexuel·le·s, bis,
transgenres et queers qui sont victimes d’homophobie ou de transphobie. Pas
nous. Je le répète, ce n’est pas un jugement, mais un constat. En admettant ses
privilèges, on accepte de **faire partie du problème**. À ce moment, ça ne
devient plus « des râleuses qui nous emmerdent avec leurs problèmes dont on n’a
rien à faire ». On sait qu’on en est aussi, qu’on participe à ça. Après, on est
libre de changer nos habitudes. D’accepter, non pas de renoncer à ces
privilèges, mais de travailler à les partager. Ce n’est bien sûr pas un acte
individuel, et je ne peux pas dire « tiens, prends ma place d’homme aux yeux de
la société ». Pas par faute d’envie, mais parce que changer mon regard n’est
qu’une pierre à l’édifice. Pour que ça ait un impact, il faut naturellement
qu’une masse critique ait conscience de ce problème, et fasse un effort pour y
remédier. Cette masse critique, c’est vous et moi, pas « les vilains
misogynes ». Admettre ça, c’est lutter pour l’égalité des genres. C’est être
féministe.

Et toi, Jérôme, qui es pour l’égalité, t’es-tu remis en question ? Ou te
contentes-tu de taper sur celles et ceux qui s’efforcent de sensibiliser les
autres ? Au contraire, ai-je envie de dire. Car quand tu dénonces le fait que le
dossier de Mar_Lard pointe du doigt « les geeks », il est clair que tu te sens
vexé, car on sous-entend que ta précieuse communauté pourrait avoir un problème,
pire, que tu pourrais en faire partie. Non content d’être du communautarisme
bien idiot, il s’agit d’un rejet pur et dur du problème. Tu préfères penser que
le sexisme, bien sûr que ça existe, mais ce sont « les autres ». Pas de ça
autour de toi, sinon tu l’aurais vu, n’est-ce pas ? Et si c’était le simple fait
de ne pas le remarquer qui te mettait mal à l’aise ? Tu sais, depuis le (faible)
temps que je milite pour l’égalité, j’ai fini par repérer les patterns courants
de ceux qui tapent sur le féminisme, mais qui sont d’accord sur le principe,
hein, **mais**… Mais faut pas crier aussi fort, mais faut pas généraliser sur
les geeks, y’en a des biens… Et là, je ne te fais que les réponses construites.
Parce que dans leur immense majorité, les réactions sont purement *haineuses*.
As-tu jeté un œil sur les commentaires du moindre article rédigé par Mar_Lard ?
Toi qui parles d’égalité, et de ses billets, n’as-tu pas trouvé pertinent, même
si tu souhaitais critiquer intelligemment, de signaler cet état de fait ?
D’ailleurs, qu’en penses-tu, toi qui, sans le vouloir, participes à la levée de
boucliers qui suit inévitablement ce pavé dans la mare ?

Non pas que le dossier dont il est question ne soit pas critiquable. Au
contraire. Il s’agit du travail personnel de quelqu’un de passionnée par le
sujet, et dont ce n’est pas le métier. Ça se ressent, évidemment. Mais on va lui
reprocher de ne pas parler de quelque chose, d’avoir un ton incorrect. Tout est
prétexte à critiquer son texte. Ça a quelque chose de malsain, et je dois dire
que je t’en veux de ne pas avoir vu plus loin que ça.

Finalement, tu as raison, tas de pixels. C’est bien une question de binaire. La
pilule rouge ou bleue. La reconnaissance du problème ou son déni.
