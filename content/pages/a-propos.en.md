Title: About
Permalink: about

Gordon, free software geek, hacktivist, everything you need.

I’m a kind of superhero, pissing code at day, pissing free code at night, and
spending the rest of my time joking hard with servers (not the *bar* type, uh)

“Gus dans [un garage](http://laquadrature.net)”, giving a hand or a beard from
time to time. [Free software](http://april.org) enthusiast.
Full-time [gentooïst](http://gentoo.org/), and [Bépo](http://bepo.fr)-user
because we have to kill some time while compiling texlive (plus, it warms
hands). I like to help people by hosting what they need. And I’m security
paranoid (but not the kind of security you get by installing surveillance
cameras everywhere). I happen to work with [some guys](http://telecomix.org)
who spread datalove everywhere.

With all that, I find time to build [IP address factories](http://ndn-fai.fr),
children of the venerable [FDN](http://fdn.fr). And since we wanted to build
free modems, we launched the [Nicelab](http://nicelab.org), the hackerspace
that smells the sunshine.

As I’m pretty old school, if you want to contact me, you have more chances to
join me by email than on any crappy-social-website. It’s quite easy to find an
address on which I reply, for example the “contact” account on this domain
name. And use this [GPG key](/files/damien.asc) to encrypt the message. Be
kind, and verify the key’s fingerprint:

    1633 367B 013F 9F61 B75E BE62 23B8 17F1 8593 65CC
